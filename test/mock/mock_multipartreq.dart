import 'package:http/http.dart';
import 'package:mockito/mockito.dart';
import 'package:simas/widget/multipartreq_provider.dart';

class MockMultiPartReq extends Fake implements BaseMultiPartReq {
  @override
  sendRequest(MultipartRequest request) async {
    var response = Response('{}', 200);
    return  response;
  }
}