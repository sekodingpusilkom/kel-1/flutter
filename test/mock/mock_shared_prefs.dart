import 'package:flutter_test/flutter_test.dart';
import 'package:simas/widget/sharedprefs_provider.dart';

class MockSharedPrefs extends Fake implements BaseSharedPrefs{
  @override
  Future<String> getString(String key) async {
    if(key == "userId"){
      return "1";
    }
    else if(key == "token"){
      return "exampletoken";
    }
    return "";
  }
}
