// ignore_for_file: import_of_legacy_library_into_null_safe
import 'package:flutter_test/flutter_test.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:simas/widget/login/gps_checker.dart';

class MockGpsLocationImpl extends Fake implements BaseGpsLocation {
  @override
  Future<bool> isLocationServiceEnabled() async {
    return true;
  }

  @override
  Future<LocationPermission> checkPermission() async {
    return LocationPermission.always;
  }

  @override
  Future<Position> getCurrentPosition() async {
    return Position(longitude: 35.33, latitude: 35.33, timestamp: DateTime.now(), accuracy: 35, altitude: 35, heading: 35.33, speed: 33, speedAccuracy: 33);
  }
  @override
  Future<dynamic> findAddressesFromCoordinates(dynamic latitude, dynamic longitude) async{
    Address ret = new Address(coordinates: Coordinates(35.53,35.55),addressLine: "sdf",adminArea: "asdf",countryCode: "asd",countryName: "cd",featureName: "dksf",locality: "sdf",postalCode: "sfsdf",subAdminArea: "sdfsdf",subLocality: "sdf",subThoroughfare: "sdf",thoroughfare: "sdfsf");
    return [ret];
  }
}