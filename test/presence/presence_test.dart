


import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:simas/models/commons/exception_constant.dart';
import 'package:simas/widget/multipartreq_provider.dart';
import 'package:simas/widget/presence/presence_bloc.dart';
import 'package:simas/widget/presence/presence_event.dart';
import 'package:simas/widget/presence/presence_state.dart';
import 'package:simas/widget/sharedprefs_provider.dart';

import '../mock/mock_multipartreq.dart';
import '../mock/mock_shared_prefs.dart';

void main() {
  setUpAll(() {
    final _getIt = GetIt.instance;

    _getIt.registerLazySingleton<BaseMultiPartReq>(() => MockMultiPartReq());
    _getIt.registerLazySingleton<BaseSharedPrefs>(() => MockSharedPrefs());
  });

  aktivitasUplBloc();
}

void aktivitasUplBloc() {
  group('Presence Bloc Test', () {
    blocTest<PresenceBloc, PresenceState>(
      'emits presence upload to fail if object is empty',
      build: () {
        return PresenceBloc();
      },
      act: (bloc) => bloc.add(PresenceWithButtonPressed("", "", 0,0, [],0)),
      expect: () => [PresenceLoading(),PresenceFailure(ExceptionConstant.PRES_UPL_FAILED)],
    );

    blocTest<PresenceBloc, PresenceState>(
      'emits presence upload to success if object sent',
      build: () {
        return PresenceBloc();
      },
      act: (bloc) => bloc.add(PresenceWithButtonPressed("1", "assets/images/Sinarmas.png", 0,0, [3,2],0)),
      expect: () => [PresenceLoading(),PresenceSuccess(),PresenceInitial()],
    );
  });
}
