import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:mockito/mockito.dart';
import 'package:simas/models/commons/exception_constant.dart';
import 'package:simas/widget/multipartreq_provider.dart';
import 'package:simas/widget/sharedprefs_provider.dart';
import 'package:simas/widget/vaksinasi/bloc/vaksinasi_bloc.dart';

class MockMultiPartReq extends Fake implements BaseMultiPartReq {
  @override
  sendRequest(MultipartRequest request) async {
    var response = Response('{}', 200);
    return  response;
  }
}

void main() {
  var reqObj = MockMultiPartReq();
  setUpAll(() {
    final _getIt = GetIt.instance;
    _getIt.registerLazySingleton<BaseMultiPartReq>(() => reqObj);
    _getIt.registerLazySingleton<BaseSharedPrefs>(() => SharedPrefsImpl());
  });
  vaksinasiBloc();
}

void vaksinasiBloc() {
  group('Vaksinasi Bloc Test', () {
    blocTest<VaksinasiBloc, VaksinasiState>(
      'emits vaksinasi upload to fail if object is empty',
      build: () {
        return VaksinasiBloc();
      },
      act: (bloc) => bloc.add(VaksinasiPostData(userId: "1",date: "",file:  "",number:  "",vaccineType:  "")),
      expect: () => [VaksinasiStateLoading(),VaksinasiStateFailure(ExceptionConstant.VAKSINASI_FAILED)],
    );

    blocTest<VaksinasiBloc, VaksinasiState>(
      'emits vaksinasi upload to success if object sent',
      build: () {
        return VaksinasiBloc();
      },
      act: (bloc) => bloc.add(VaksinasiPostData(userId: "1",date: "2021-08-11",file:  "assets/images/Sinarmas.png",number: "1",vaccineType:  "1")),
      expect: () => [VaksinasiStateLoading(),VaksinasiStateSuccess()],
    );
  });
}
