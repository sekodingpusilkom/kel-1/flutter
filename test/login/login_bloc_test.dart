// @dart=2.9
import 'dart:convert';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:mockito/mockito.dart';
import 'package:simas/widget/authentication/authentication_bloc.dart';
import 'package:simas/widget/authentication/authentication_service.dart';
import 'package:simas/widget/login/login_bloc.dart';
import 'package:simas/widget/login/login_event.dart';
import 'package:simas/widget/login/login_state.dart';
import 'package:simas/models/commons/url_constant.dart' as ConstantUrl;
import '../widget/aktivitas/aktivitas_contain_with_data_test.mocks.dart';

void main() {
  var client = MockClient();
  setUpAll(() {
    final _getIt = GetIt.instance;

    _getIt.registerLazySingleton<Client>(() => client);
  });
  when(client.post(Uri.parse(ConstantUrl.LOGIN),
      headers: {
        "content-type": "application/json",
        "accept": "application/json",
      },
      body: json.encode({'nik': "M001", 'password': "password"})))
      .thenAnswer((_) async {
    return Future.value(Response('''{
          "user_id": "1",
          "token": "exampletoken"
      }''', 200));
  });

  when(client.post(Uri.parse(ConstantUrl.LOGIN),
      headers: {
        "content-type": "application/json",
        "accept": "application/json",
      },
      body: json.encode({'nik': "M001", 'password': "wrongpwd"})))
      .thenAnswer((_) async {
    return Future.value(Response('''{{
    "timestamp": "2021-08-11T07:58:25.505+00:00",
    "status": 401,
    "error": "Unauthorized",
    "path": "/api/login"
    }''', 401));
  });

  loginBloc(client);
}

void loginBloc(Client client) {
  group('LoginBloc Test', () {


    blocTest<LoginBloc, LoginState>(
      'emits login success when nik and password true',
      build: () {
        final authService = AuthenticationServiceImpl();
        final authBloc = AuthenticationBloc(authService);
        return LoginBloc(authBloc, authService);
      },
      act: (bloc) => bloc.add(LoginInWithNikButtonPressed(nik:"M001",password: "password")),
      expect: () => [LoginStateLoading(),LoginStateSuccess()],
    );

    blocTest<LoginBloc, LoginState>(
      'emits login failure when nik or password false',
      build: () {
        final authService = AuthenticationServiceImpl();
        final authBloc = AuthenticationBloc(authService);
        return LoginBloc(authBloc, authService);
      },
      act: (bloc) => bloc.add(LoginInWithNikButtonPressed(nik:"M001",password: "wrongpwd")),
      expect: () => [LoginStateLoading(),LoginStateFailure("NIK atau password yang anda masukkan salah")],
    );

  });
}
