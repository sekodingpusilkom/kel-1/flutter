// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:simas/widget/login/gps_checker.dart';
import 'package:simas/widget/presence/presence_page.dart';
import 'package:simas/widget/sharedprefs_provider.dart';

import '../../mock/mock_client.dart';
import '../../mock/mock_gpslocation.dart';
import '../../mock/mock_shared_prefs.dart';

void main() {
  setUpAll(() {
    final _getIt = GetIt.instance;

    _getIt.registerLazySingleton<BaseGpsLocation>(() => MockGpsLocationImpl());
    _getIt.registerLazySingleton<Client>(() => MockClient());
    _getIt.registerLazySingleton<BaseSharedPrefs>(() => MockSharedPrefs());

  });

  testWidgets('presence contain ...', (tester) async {
    Widget testWidget() {
      return MediaQuery(
        data: new MediaQueryData(),
        child: new MaterialApp(home: PresencePage()),
      );
    }

    await tester.pumpWidget(testWidget());
    await tester.pump();
    await tester.pumpAndSettle();
    final nameFinder = find.text('Foto Selfie');
    expect(nameFinder, findsOneWidget);
    final tombolFinder = find.byKey(Key("submitPresence"));
    expect(tombolFinder, findsOneWidget);
    await tester.tap(tombolFinder);
    await tester.pump();
    await tester.pumpAndSettle();
    final alertFinder = find.text('Mohon ambil foto selfie');
    expect(alertFinder, findsOneWidget);
  });
}
