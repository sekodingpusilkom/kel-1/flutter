// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';

import 'package:mockito/mockito.dart';
import 'package:simas/widget/riwayat_vaksinasi/riwayat_vaksinasi_page.dart';
import 'package:simas/widget/sharedprefs_provider.dart';
import 'package:simas/models/commons/url_constant.dart' as ConstantUrl;
import '../../mock/mock_client.dart';
import '../../mock/mock_shared_prefs.dart';
void main() {
  var client = MockClient();
  setUpAll(() {
    final _getIt = GetIt.instance;

    _getIt.registerLazySingleton<Client>(() => client);
    _getIt.registerLazySingleton<BaseSharedPrefs>(() => MockSharedPrefs());
  });
  when(client.get(Uri.parse(ConstantUrl.VACCINE + "/1"),
      headers: {
        "content-type": "application/json",
        "accept": "application/json",
        "authorization": "Bearer exampletoken"
      }))
      .thenAnswer((_) async {
    return Future.value(Response('''[]''', 200));
  });

  testWidgets('Vaksinasi test', (WidgetTester tester) async {
    // Build our app and trigger a frame.

    Widget testWidget() {
      return MediaQuery(
        data: new MediaQueryData(),
        child: new MaterialApp(home: RiwayatVaksinasiPage()),
      );
    }

    // emit error if image not filled
    await tester.pumpWidget(testWidget());
    await tester.pump();
    await tester.pumpAndSettle();
    var noDataFinder = find.text('Belum ada Riwayat Vaksinasi Terkini');
    expect(noDataFinder, findsOneWidget);

  });
}
