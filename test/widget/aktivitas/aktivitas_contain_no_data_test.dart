// ignore_for_file: unused_local_variable
// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:simas/widget/aktivitas/aktivitas_page.dart';
import 'package:simas/models/commons/url_constant.dart' as ConstantUrl;

import 'aktivitas_contain_no_data_test.mocks.dart';


@GenerateMocks([Client])
void main() {
  testWidgets('Shows no activity if response is empty', (WidgetTester tester) async {
    var client = MockClient();
    final _getIt = GetIt.instance;
    when(client.get(Uri.parse(ConstantUrl.EXERCISE),
    headers: {
        "content-type": "application/json",
        "accept": "application/json",
        "authorization": "Bearer exampletoken"
        }))
        .thenAnswer((_) async {
      return Future.value(Response('[]', 200));
    });
    _getIt.registerLazySingleton<Client>(
            () => client);

    Widget testWidget() {
      return MediaQuery(
        data: new MediaQueryData(),
        child: new MaterialApp(home: AktivitasPage()),
      );
    }

    await tester.pumpWidget(testWidget());
    await tester.pump();
    await tester.pumpAndSettle();
    var noDataFinder = find.text('Belum ada Aktivitas Kebugaran Terkini');
    expect(noDataFinder, findsOneWidget);
  }
  );
}
