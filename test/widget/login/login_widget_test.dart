// @dart=2.9
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:mockito/mockito.dart';
import 'package:simas/widget/authentication/authentication_bloc.dart';
import 'package:simas/widget/authentication/authentication_event.dart';
import 'package:simas/widget/authentication/authentication_service.dart';
import 'package:simas/widget/login/gps_checker.dart';
import 'package:simas/widget/my_app.dart';
import 'package:simas/models/commons/url_constant.dart' as ConstantUrl;

import '../../mock/mock_client.dart';
import '../../mock/mock_gpslocation.dart';


void main() {
  var client = MockClient();
  setUpAll(() {
    final _getIt = GetIt.instance;

    _getIt.registerLazySingleton<BaseGpsLocation>(() => MockGpsLocationImpl());

    _getIt.registerLazySingleton<Client>(() => client);
  });

  testWidgets('Login test', (WidgetTester tester) async {
      when(client.post(Uri.parse(ConstantUrl.LOGIN),
            headers: {
              "content-type": "application/json",
              "accept": "application/json",
            },
            body: json.encode({'nik': "M001", 'password': "password"})))
        .thenAnswer((_) async {
      return Future.value(Response('''{
          "user_id": "1",
          "token": "exampletoken"
      }''', 200));
    });
    await tester.pumpWidget(RepositoryProvider<AuthenticationService>( // inisialisasi awal
      create: (context) {
        return AuthenticationServiceImpl();
      },
      child: BlocProvider<AuthenticationBloc>(
        create: (context) {
          final authService =
              RepositoryProvider.of<AuthenticationService>(context);
          return AuthenticationBloc(authService)..add(AppLoaded());
        },
        child: MyApp(),
      ),
    ));

    await tester.pump();
    await tester.pumpAndSettle();
    await tester.tap(find.byKey(Key("login")));
    await tester.pump();
    await tester.pumpAndSettle();
    final finder = find.text('NIK diperlukan.');
    final finder2 = find.text('Password minimal 8 karakter');
    expect(finder, findsOneWidget);
    expect(finder2, findsOneWidget);

    await tester.enterText(find.byKey(Key("nik")), "M001");
    await tester.enterText(find.byKey(Key("password")), "password");
    await tester.tap(find.byKey(Key("login")));
    await tester.pump();
    await tester.pumpAndSettle();
    final finderHome = find.byKey(Key("homeMenu"));
    expect(finderHome, findsOneWidget);

  });
}
