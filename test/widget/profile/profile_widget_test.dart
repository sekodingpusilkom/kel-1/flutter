// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:mockito/mockito.dart';
import 'package:simas/widget/profile/profile_contain.dart';
import 'package:simas/widget/sharedprefs_provider.dart';

import '../../mock/mock_shared_prefs.dart';
import '../aktivitas/aktivitas_contain_no_data_test.mocks.dart';
import 'package:simas/models/commons/url_constant.dart' as ConstantUrl;

void main() {
  var client = MockClient();
  setUpAll(() {
    final _getIt = GetIt.instance;
    _getIt.registerLazySingleton<Client>(() => client);
    _getIt.registerLazySingleton<BaseSharedPrefs>(() => MockSharedPrefs());
  });

  testWidgets('profile contain ...', (tester) async {
    when(client.get(Uri.parse(ConstantUrl.USER_DETAIL + "/1"),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
          "authorization": "Bearer exampletoken"
        }))
        .thenAnswer((_) async {
      return Future.value(Response('''{
          "name": "VINCENT"
      }''', 200));
    });

    Widget testWidget() {
      return MediaQuery(
        data: new MediaQueryData(),
        child: new MaterialApp(home: ProfileContain()),
      );
    }

    await tester.pumpWidget(testWidget());
    await tester.pump();
    await tester.pumpAndSettle();
    final nameFinder = find.text('VINCENT');
    expect(nameFinder, findsOneWidget);

    final pwdFinder = find.text('Ganti password');
    expect(pwdFinder, findsOneWidget);
  });
}