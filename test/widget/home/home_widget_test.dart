// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simas/widget/home/home_page.dart';


void main() {
  testWidgets('Home test', (WidgetTester tester) async {
    // Build our app and trigger a frame.

    Widget testWidget() {
      return MediaQuery(
        data: new MediaQueryData(),
        child: new MaterialApp(home: MyHomePage()),
      );
    }

    await tester.pumpWidget(testWidget());
    await tester.pump();
    await tester.pumpAndSettle();
    var riwayatMenu = find.text("Riwayat");
    expect(riwayatMenu, findsOneWidget);
    var aktGrid = find.text("Aktivitas");
    expect(aktGrid, findsOneWidget);

    var berandaGrid = find.text("Beranda");
    expect(berandaGrid, findsOneWidget);

    var ijinGrid = find.text("Ijin / Sakit");
    expect(ijinGrid, findsOneWidget);

    var profilGrid = find.text("Profil");
    expect(profilGrid, findsOneWidget);
  });
}
