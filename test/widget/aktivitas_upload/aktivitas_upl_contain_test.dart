
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simas/widget/aktivitas_upload/aktivitas_upl_page.dart';


void main() {
  testWidgets('Aktivitas Upload test', (WidgetTester tester) async {
    // Build our app and trigger a frame.

    Widget testWidget() {
      return MediaQuery(
        data: new MediaQueryData(),
        child: new MaterialApp(home: AktivitasUplPage()),
      );
    }

    await tester.pumpWidget(testWidget());
    await tester.pump();
    await tester.pumpAndSettle();
    var uploadBtn = find.byKey(Key("upload"));
    expect(uploadBtn, findsOneWidget);

    // emit error if image not filled
    await tester.enterText(find.byKey(Key("description")), "abcdefghijk");
    await tester.tap(uploadBtn);
    await tester.pump();
    await tester.pumpAndSettle();
    var photoValidatorFinder = find.text('Mohon ambil foto aktivitas kebugaran.');
    expect(photoValidatorFinder, findsOneWidget);

  });
}
