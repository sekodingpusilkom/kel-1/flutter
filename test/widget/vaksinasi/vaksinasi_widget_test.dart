// @dart=2.9
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:mockito/mockito.dart';
import 'package:simas/widget/sharedprefs_provider.dart';
import 'package:simas/widget/vaksinasi/vaksinasi_page.dart';

import '../../mock/mock_shared_prefs.dart';
import '../aktivitas/aktivitas_contain_no_data_test.mocks.dart';
import 'package:simas/models/commons/url_constant.dart' as ConstantUrl;

void main() {
  var client = MockClient();
  setUpAll(() {
    final _getIt = GetIt.instance;

    _getIt.registerLazySingleton<Client>(() => client);
    _getIt.registerLazySingleton<BaseSharedPrefs>(() => MockSharedPrefs());
  });
  when(client.get(Uri.parse(ConstantUrl.VACCINE_TYPE),
      headers: {
        "content-type": "application/json",
        "accept": "application/json",
        "authorization": "Bearer exampletoken"
      }))
      .thenAnswer((_) async {
    return Future.value(Response('''[
    {
        "id": 951,
        "name": "pfizer"
    },
    {
        "id": 1001,
        "name": "sinopharm"
    }]''', 200));
  });

  testWidgets('Vaksinasi test', (WidgetTester tester) async {
    // Build our app and trigger a frame.

    Widget testWidget() {
      return MediaQuery(
        data: new MediaQueryData(),
        child: new MaterialApp(home: VaksinasiPage()),
      );
    }

    await tester.pumpWidget(testWidget());
    await tester.pump();
    await tester.pumpAndSettle();
    var submitBtn = find.byKey(Key("submitBtn"));
    expect(submitBtn, findsOneWidget);

    // emit error if image not filled
    await tester.tap(find.byKey(Key("submitBtn")));
    await tester.pump();
    await tester.pumpAndSettle();
    var photoValidatorFinder = find.text('Mohon isi vaksin ke berapa');
    expect(photoValidatorFinder, findsOneWidget);

  });
}
