import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:mockito/mockito.dart';
import 'package:simas/models/commons/exception_constant.dart';
import 'package:simas/widget/aktivitas_upload/aktivitas_upl_bloc.dart';
import 'package:simas/widget/aktivitas_upload/aktivitas_upl_event.dart';
import 'package:simas/widget/aktivitas_upload/aktivitas_upl_state.dart';
import 'package:simas/widget/multipartreq_provider.dart';

class MockMultiPartReq extends Fake implements BaseMultiPartReq {
  @override
  sendRequest(MultipartRequest request) async {
    var response = Response('{}', 200);
    return  response;
  }
}

void main() {

  var reqObj = MockMultiPartReq();
  setUpAll(() {
    final _getIt = GetIt.instance;

    _getIt.registerLazySingleton<BaseMultiPartReq>(() => reqObj);
  });

  aktivitasUplBloc();
}

void aktivitasUplBloc() {
  group('Aktivitas Upload Bloc Test', () {
    blocTest<AktivitasUplBloc, AktivitasUplState>(
      'emits activity upload to fail if object is empty',
      build: () {
        return AktivitasUplBloc();
      },
      act: (bloc) => bloc.add(AktivitasUplButtonPressed("", "", "")),
      expect: () => [AktivitasUplLoading(),AktivitasUplFailure(ExceptionConstant.AKT_UPL_FAILED)],
    );

    blocTest<AktivitasUplBloc, AktivitasUplState>(
      'emits activity upload to success if object sent',
      build: () {
        return AktivitasUplBloc();
      },
      act: (bloc) => bloc.add(AktivitasUplButtonPressed("1", "assets/images/Sinarmas.png", "description")),
      expect: () => [AktivitasUplLoading(),AktivitasUplSuccess()],
    );
  });
}
