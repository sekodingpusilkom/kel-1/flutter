import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simas/widget/aktivitas/aktivitas_bloc.dart';
import 'package:simas/widget/aktivitas/aktivitas_event.dart';
import 'package:simas/widget/aktivitas_upload/aktivitas_state.dart';

void main() {
  aktivitasBloc();
}

void aktivitasBloc() {
  group('Aktivitas Upload Bloc Test', () {
    blocTest<AktivitasBloc, AktivitasState>(
      'emits activity to success if object is image is found',
      build: () {
        return AktivitasBloc();
      },
      act: (bloc) => bloc.add(AktivitasSharePressed(
          imgUrl:
              "https://i0.wp.com/buckssport.org/wp-content/uploads/2021/01/Tips-for-Protecting-Badminton-Racket-Victor-Sport-Online_800x800.jpg?w=640&ssl=1",
          desc: "test")),
      expect: () => [AktivitasLoading(), AktivitasShareSuccess("", "")],
    );

    blocTest<AktivitasBloc, AktivitasState>(
      'emits activity to fail if object is image is not found',
      build: () {
        return AktivitasBloc();
      },
      act: (bloc) => bloc.add(AktivitasSharePressed(
          imgUrl: "https://nzsdkfsldk.com/", desc: "test")),
      expect: () => [AktivitasLoading(), AktivitasFailure("")],
    );
  });
}
