class User{
  final String nik;
  final String name;

  User(this.nik,this.name);

  @override
  String toString() {
    return 'User{nik: $nik, name: $name}';
  }
}