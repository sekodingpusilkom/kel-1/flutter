const String HOST_URL = "http://35.212.240.171";

const String LOGIN = HOST_URL + "/api/login";
const String CLOCK_IN = HOST_URL + "/api/clockin";
const String CLOCK_OUT = HOST_URL + "/api/clockout";
const String IS_CLOCK_IN = HOST_URL + "/api/attendance";
const String IS_FILL_MENTAL = HOST_URL + "/api/condition/isFilled";
const String FILL_DETAIL = HOST_URL + "/api/condition/detail";
const String EXERCISE = HOST_URL + "/api/exercise";
const String VACCINE = HOST_URL + "/api/vaccination";
const String VACCINE_DETAIL = HOST_URL + "/api/vaccination/detail";
const String VACCINE_TYPE = HOST_URL + "/api/vaccine-type";
const String USER_DETAIL = HOST_URL + "/api/users";
const String IMG_EXERCISE_URL = HOST_URL + "/file/resources/exercise/";
const String FILE_VACCINE_URL = HOST_URL + "/file/resources/vaccination/";
const String CHANGE_PASSWORD = HOST_URL + "/api/changePassword/";

const List<String> PRESENCE = [CLOCK_IN, CLOCK_OUT];

Uri historyClockIn(String? userId, String startDate, String endDate) {
  return Uri.parse("$HOST_URL/api/attendance/all/$userId/$startDate/$endDate");
}
