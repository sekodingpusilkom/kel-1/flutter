
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';

// style: ElevatedButton.styleFrom(
// primary: Color.fromRGBO(250, 64, 113, 1),
// minimumSize: Size(80.0, 45.0),
// shape: new RoundedRectangleBorder(
// borderRadius: new BorderRadius.circular(5.0)))

AlertDialog alertDialog(BuildContext context, String message,bool isPermission) {
  var widgetList = <Widget>[TextButton(
      child: Text('OK', style: TextStyle(color: Color.fromRGBO(250, 64, 113, 1))),
      onPressed: () => Navigator.of(context).pop(),
  )];
  if (isPermission) {
    widgetList = <Widget>[
    TextButton(
        child: Text('Buka pengaturan', style: TextStyle(color: Color.fromRGBO(250, 64, 113, 1))),
        onPressed: () async {
          await openAppSettings();
          Navigator.of(context).pop();
        },
      ),
      TextButton(
        child: Text('Batal', style: TextStyle(color: Color.fromRGBO(250, 64, 113, 1))),
        onPressed: () async {
          Navigator.of(context).pop();
          SystemChannels.platform.invokeMethod('SystemNavigator.pop');
        },
      ),
    ];
  }

  AlertDialog alert = new AlertDialog(
    content: new Text(message),
    actions: widgetList,
  );
  return alert;
}

void showDialogBox(BuildContext context, String message, {bool permission = false}) {
  AlertDialog alert = alertDialog(context, message,permission);
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
