class ExceptionConstant{
  static const String GEN_FAILED_LOAD = "gagal memuat, coba lagi nanti";
  static const String GEN_DATA_REQUIRED = "data yang dikirim kurang lengkap, coba lengkapi";

  static const String AKT_FAILED_EDIT = "aktivitas kebugaran gagal diedit, coba lagi nanti";
  static const String AKT_FAILED_DEL = "aktivitas kebugaran gagal dihapus, coba lagi nanti";
  static const String AKT_FAILED_SHR = "aktivitas kebugaran gagal dibagi, coba lagi nanti";
  static const String AKT_SUCCESS_EDIT = "aktivitas kebugaran berhasil diedit";
  static const String AKT_SUCCESS_DEL = "aktivitas kebugaran berhasil dihapus";
  static const String AKT_SUCCESS_SHR = "aktivitas kebugaran berhasil dibagi";
  static const String AKT_NOT_FOUND = "data aktivitas kebugaran tidak ditemukan";

  static const String AKT_UPL_FAILED = "data aktivitas kebugaran gagal diupload";
  static const String VAKSINASI_FAILED = "data vaksinasi gagal diupload";
  static const String VAKSINASI_SUC = "data vaksinasi berhasil diupload";

  static const String PRES_UPL_FAILED = "data presenci gagal diupload, coba lagi nanti";

  static const String VAKSINASI_TYPE_FAILED = "tipe vaksin gagal di-load";
  static const String RIWAYAT_VAKSINASI_FAILED = "riwayat vaksinasi gagal di-load";
  static const String SESSION_EXPIRED = "sesi anda telah habis, silahkan login kembali";

}