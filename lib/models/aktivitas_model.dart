import 'dart:convert';

List<AktivitasModel> aktivitasModelFromJson(String str) =>
    List<AktivitasModel>.from(
        json.decode(str).map((x) => AktivitasModel.fromJson(x)));

class AktivitasModel {
  AktivitasModel({
    required this.id,
    required this.usersId,
    required this.name, // ??
    required this.photo,
    required this.description,
  });

  int id;
  String photo;
  String usersId;
  String name;
  String description;

  factory AktivitasModel.fromJson(Map<String, dynamic> json) => AktivitasModel(
        id: json["id"],
        usersId: json["users_id"].toString(),
        name: json["name"],
        photo: json["photo"],
        description: json["description"],
      );
}
