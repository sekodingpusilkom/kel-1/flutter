// To parse this JSON data, do
//
//     final historyClockIn = historyClockInFromJson(jsonString);

import 'dart:convert';

List<HistoryClockIn> historyClockInFromJson(String str) =>
    List<HistoryClockIn>.from(
        json.decode(str).map((x) => HistoryClockIn.fromJson(x)));

String historyClockInToJson(List<HistoryClockIn> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class HistoryClockIn {
  HistoryClockIn({
    required this.id,
    required this.users,
    required this.startTime,
    required this.endTime,
    required this.latStart,
    required this.longStart,
    required this.latEnd,
    required this.longEnd,
    required this.photoStart,
    required this.photoEnd,
  });

  int id;
  Users users;
  DateTime startTime;
  DateTime endTime;
  double latStart;
  double longStart;
  double latEnd;
  double longEnd;
  String photoStart;
  String photoEnd;

  factory HistoryClockIn.fromJson(Map<String, dynamic> json) => HistoryClockIn(
        id: json["id"],
        users: Users.fromJson(json["users"]),
        startTime: DateTime.parse(json["startTime"]).toUtc().toLocal(),
        endTime: DateTime.parse(json["endTime"]).toUtc().toLocal(),
        latStart: json["lat_start"].toDouble(),
        longStart: json["long_start"].toDouble(),
        latEnd: json["lat_end"].toDouble(),
        longEnd: json["long_end"].toDouble(),
        photoStart: json["photo_start"],
        photoEnd: json["photo_end"] == null ? null : json["photo_end"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "users": users.toJson(),
        "startTime": startTime.toIso8601String(),
        "endTime": endTime.toIso8601String(),
        "lat_start": latStart,
        "long_start": longStart,
        "lat_end": latEnd,
        "long_end": longEnd,
        "photo_start": photoStart,
        "photo_end": photoEnd,
      };
}

class Users {
  Users({
    required this.id,
    required this.nik,
    required this.email,
    required this.password,
    required this.name,
    required this.phone,
    required this.role,
    required this.division,
    required this.enabled,
    required this.authorities,
    required this.accountNonLocked,
    required this.accountNonExpired,
    required this.credentialsNonExpired,
    required this.username,
  });

  int id;
  String nik;
  String email;
  String password;
  String name;
  String phone;
  Division role;
  Division division;
  bool enabled;
  List<dynamic> authorities;
  bool accountNonLocked;
  bool accountNonExpired;
  bool credentialsNonExpired;
  String username;

  factory Users.fromJson(Map<String, dynamic> json) => Users(
        id: json["id"],
        nik: json["nik"],
        email: json["email"],
        password: json["password"],
        name: json["name"],
        phone: json["phone"],
        role: Division.fromJson(json["role"]),
        division: Division.fromJson(json["division"]),
        enabled: json["enabled"],
        authorities: List<dynamic>.from(json["authorities"].map((x) => x)),
        accountNonLocked: json["accountNonLocked"],
        accountNonExpired: json["accountNonExpired"],
        credentialsNonExpired: json["credentialsNonExpired"],
        username: json["username"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nik": nik,
        "email": email,
        "password": password,
        "name": name,
        "phone": phone,
        "role": role.toJson(),
        "division": division.toJson(),
        "enabled": enabled,
        "authorities": List<dynamic>.from(authorities.map((x) => x)),
        "accountNonLocked": accountNonLocked,
        "accountNonExpired": accountNonExpired,
        "credentialsNonExpired": credentialsNonExpired,
        "username": username,
      };
}

class Division {
  Division({
    required this.id,
    required this.name,
  });

  int id;
  String name;

  factory Division.fromJson(Map<String, dynamic> json) => Division(
        id: json["id"],
        name: json["name"] == null ? null : json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
      };
}
