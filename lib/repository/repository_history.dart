import 'package:simas/models/history.dart';
import 'package:simas/services/history_api_provider.dart';

class HistoryRepository {
  HistoryApiProvider _historyApiProvider = HistoryApiProvider();

  Future<List<HistoryClockIn>> fetchHistoryClockIn(
          String? userId, DateTime startDate, DateTime endDate) =>
      _historyApiProvider.fetchHistoryClockIn(userId, startDate, endDate);
}
