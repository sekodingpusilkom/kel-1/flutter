import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:simas/widget/login/gps_checker.dart';
import 'package:simas/widget/multipartreq_provider.dart';
import 'package:simas/widget/sharedprefs_provider.dart';

class AppGetIt {
  final _getIt = GetIt.instance;

  void initialize() {
    _getIt
        .registerLazySingleton<BaseGpsLocation>(() => GpsLocationImpl());

    _getIt
        .registerLazySingleton<Client>(() => new Client());

    _getIt
        .registerLazySingleton<BaseMultiPartReq>(() => new MultiPartReqImpl());

    _getIt
        .registerLazySingleton<BaseSharedPrefs>(() => new SharedPrefsImpl());
  }
}