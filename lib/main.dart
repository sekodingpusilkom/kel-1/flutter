import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:simas/data/local/local_database.dart';
import 'package:simas/get_it.dart';
import 'package:simas/widget/authentication/authentication_bloc.dart';
import 'package:simas/widget/authentication/authentication_event.dart';
import 'package:simas/widget/authentication/authentication_service.dart';
import 'package:simas/widget/my_app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await LocalDatabase.init();
  await initializeDateFormatting('id_ID', null);

  AppGetIt().initialize();
  runApp(RepositoryProvider<AuthenticationService>(
    create: (context) {
      return AuthenticationServiceImpl();
    },
    // Injects the Authentication BLoC
    child: BlocProvider<AuthenticationBloc>(
      create: (context) {
        final authService =
            RepositoryProvider.of<AuthenticationService>(context);
        return AuthenticationBloc(authService)..add(AppLoaded());
      },
      child: MyApp(),
    ),
  ));
}
