import 'dart:convert';

import 'package:http/http.dart';

abstract class BaseClient{
  Future<Response> get(Uri url, {Map<String, String>? headers});
  Future<Response> post(Uri url, {Map<String, String>? headers, Object? body, Encoding? encoding});
}

class ClientImpl implements BaseClient{
  Client client = new Client();
  @override
  Future<Response> get(Uri url, {Map<String, String>? headers})async{
    return await client.get(url, headers: headers);
  }

  @override
  Future<Response> post(Uri url, {Map<String, String>? headers, Object? body, Encoding? encoding}) {
    return client.post(url, headers: headers, body: body, encoding: encoding);
  }

  ClientImpl(){
    print("client impl made");
  }
}

