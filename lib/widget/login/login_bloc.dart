import 'package:http/http.dart' show Client;
import 'package:bloc/bloc.dart';
import 'package:simas/widget/authentication/authentication_bloc.dart';
import 'package:simas/widget/authentication/authentication_event.dart';
import 'package:simas/widget/authentication/authentication_exception.dart';
import 'package:simas/widget/authentication/authentication_service.dart';
import 'login_event.dart';
import 'login_state.dart';
class LoginBloc extends Bloc<LoginEvent, LoginState>{
  final AuthenticationBloc _authenticationBloc;
  final AuthenticationService _authenticationService;

  LoginBloc(AuthenticationBloc authenticationBloc, AuthenticationService authenticationService)
      : _authenticationBloc = authenticationBloc,
        _authenticationService = authenticationService,
        super(LoginStateInitial());

  Client client = Client();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginInWithNikButtonPressed) {
      yield* _mapLoginToState(event);
    }
  }
  Stream<LoginState> _mapLoginToState(LoginInWithNikButtonPressed event) async* {
    yield LoginStateLoading();
    try {
      final user = await _authenticationService.signInWithEmailAndPassword(event.nik, event.password);
      _authenticationBloc.add(UserLoggedIn(user: user));
      yield LoginStateSuccess();
    } on AuthenticationException catch (e) {
      yield LoginStateFailure(e.message);
    }
    catch (err) {
      yield LoginStateFailure('An unknown error occured');
    }

  }

}