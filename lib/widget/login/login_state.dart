abstract class LoginState {
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LoginState && runtimeType == other.runtimeType;

  @override
  int get hashCode => 0;
}

class LoginStateInitial extends LoginState {
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      super == other &&
          other is LoginStateInitial &&
          runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode;
}

class LoginStateLoading extends LoginState {
  LoginStateLoading();

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      super == other &&
          other is LoginStateLoading &&
          runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode;
}

class LoginStateSuccess extends LoginState {
  LoginStateSuccess();

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      super == other &&
          other is LoginStateSuccess &&
          runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode;
}

class LoginStateFailure extends LoginState {
  final String error;

  LoginStateFailure(this.error);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      super == other &&
          other is LoginStateFailure &&
          runtimeType == other.runtimeType &&
          error == other.error;

  @override
  int get hashCode => super.hashCode ^ error.hashCode;

}
