import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get_it/get_it.dart';
import 'package:simas/models/commons/method_common.dart';
import 'package:simas/widget/authentication/authentication_bloc.dart';
import 'package:simas/widget/authentication/authentication_event.dart';
import 'package:simas/widget/authentication/authentication_service.dart';
import 'package:simas/widget/authentication/authentication_state.dart';
import 'package:simas/widget/login/gps_checker.dart';
import 'package:simas/widget/login/login_bloc.dart';
import 'package:simas/widget/login/login_event.dart';
import 'package:simas/widget/login/login_state.dart';
import 'package:simas/widget/unavalable_page.dart';


class LoginChecker extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            minimum: const EdgeInsets.all(16),
            child: BlocBuilder<AuthenticationBloc, AuthenticationState>(
              builder: (context, state) {
                final authBloc = BlocProvider.of<AuthenticationBloc>(context);
                if (state is AuthenticationNotAuthenticated) {
                  return _AuthForm();
                }
                if (state is AuthenticationFailure) {
                  return Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(state.message),
                      TextButton(
                        child: Text('Retry',
                            style: TextStyle(
                                color: Theme.of(context).primaryColor)),
                        onPressed: () {
                          authBloc.add(AppLoaded());
                        },
                      )
                    ],
                  ));
                }
                // return splash screen
                return Center(
                  child: CircularProgressIndicator(
                    strokeWidth: 2,
                  ),
                );
              },
            )));
  }
}

class _AuthForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final authService = RepositoryProvider.of<AuthenticationService>(context);
    final authBloc = BlocProvider.of<AuthenticationBloc>(context);

    return Container(
      alignment: Alignment.center,
      child: BlocProvider<LoginBloc>(
        create: (context) => LoginBloc(authBloc, authService),
        child: _SignInForm(),
      ),
    );
  }
}

class _SignInForm extends StatefulWidget {
  @override
  __SignInFormState createState() => __SignInFormState();
}

class __SignInFormState extends State<_SignInForm> {
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  final _passwordController = TextEditingController();
  final _emailController = TextEditingController();
  bool _autoValidate = false;
  bool _isPasswordHidden = true;
  @override
  Widget build(BuildContext context) {
    final _loginBloc = BlocProvider.of<LoginBloc>(context);

    _onLoginButtonPressed() {
      if (_key.currentState!.validate()) {
        _loginBloc.add(LoginInWithNikButtonPressed(
            nik: _emailController.text, password: _passwordController.text));
      } else {
        setState(() {
          _autoValidate = true;
        });
      }
    }

    Widget imgField() {
      return Image.asset(
        'assets/images/Sinarmas.png',
        // width: 128,
      );
    }

    void togglePasswordVisibility() =>
        setState(() => _isPasswordHidden = !_isPasswordHidden);

    _checkGpsPermission() async {
      bool serviceEnabled = false;
      LocationPermission permission;

      var locationCheck = GetIt.instance.get<BaseGpsLocation>();
      serviceEnabled = await locationCheck.isLocationServiceEnabled();
      if (!serviceEnabled) {
        return Future.error('Location services are disabled.');
      }

      permission = await locationCheck.checkPermission();
      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) {
          return Future.error('Izin lokasi ditolak');
        }
      }
      if (permission == LocationPermission.deniedForever) {
        showDialogBox(context,
            "Anda perlu mengizinkan aplikasi mengakses lokasi, untuk keperluan  absensi",
            permission: true);
      }
    }

    Widget forgotPwdLink() {
      return Container(
          child: Row(
        children: <Widget>[
          Text('Lupa password?'),
          TextButton(
            child: Text(
              'Klik disini',
              style: TextStyle(color: Colors.blue),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => UnavailablePage()),
              );
            },
          )
        ],
        mainAxisAlignment: MainAxisAlignment.center,
      ));
    }

    return BlocListener<LoginBloc, LoginState>(listener: (context, state) {
      if (state is LoginStateFailure) {
        showDialogBox(context, state.error);
      }
    }, child: BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
      if (state is LoginStateLoading) {
        return Center(
          child: CircularProgressIndicator(),
        );
      }
      return Form(
        key: _key,
        autovalidateMode:
            _autoValidate ? AutovalidateMode.always : AutovalidateMode.disabled,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              imgField(),
              TextFormField(
                key: Key("nik"),
                decoration: InputDecoration(
                  labelText: 'NIK',
                  filled: true,
                  isDense: true,
                ),
                controller: _emailController,
                autocorrect: false,
                validator: (value) {
                  if (value == null || value == "") {
                    return 'NIK diperlukan.';
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 12,
              ),
              TextFormField(
                key: Key("password"),
                decoration: InputDecoration(
                    labelText: 'Password',
                    filled: true,
                    isDense: true,
                    suffixIcon: IconButton(
                      icon: _isPasswordHidden
                          ? Icon(Icons.visibility_off)
                          : Icon(Icons.visibility_sharp),
                      onPressed: togglePasswordVisibility,
                    )),
                obscureText: _isPasswordHidden,
                controller: _passwordController,
                validator: (value) {
                  if (value == null || value == "" || value.length < 8) {
                    return 'Password minimal 8 karakter';
                  }
                  return null;
                },
              ),
              const SizedBox(
                height: 16,
              ),
              ElevatedButton(
                key: Key("login"),
                style: ElevatedButton.styleFrom(
                    primary: Theme.of(context).primaryColor,
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(8.0))),
                child: Text(
                  'LOG IN',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: state is LoginStateLoading
                    ? () {}
                    : () async {
                        await _checkGpsPermission();
                        _onLoginButtonPressed();
                      },
              ),
              forgotPwdLink()
            ],
          ),
        ),
      );
    }));
  }
}
