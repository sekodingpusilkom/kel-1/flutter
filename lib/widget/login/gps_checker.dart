// ignore_for_file: import_of_legacy_library_into_null_safe
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';

abstract class BaseGpsLocation{
  Future<bool> isLocationServiceEnabled();
  Future<LocationPermission> checkPermission();
  Future<Position> getCurrentPosition();
  Future<dynamic> findAddressesFromCoordinates(dynamic latitude, dynamic longitude);
}

class GpsLocationImpl implements BaseGpsLocation{
  @override
  Future<bool> isLocationServiceEnabled()async{
    return await Geolocator.isLocationServiceEnabled();
  }

  @override
  Future<LocationPermission> checkPermission() async{
    return await Geolocator.checkPermission();
  }

  @override
  Future<Position> getCurrentPosition() async {
    return await Geolocator.getCurrentPosition();
  }
  @override
  Future<dynamic> findAddressesFromCoordinates(dynamic latitude, dynamic longitude) async{
    return await Geocoder.local.findAddressesFromCoordinates(
        Coordinates(latitude, longitude));
  }
}

