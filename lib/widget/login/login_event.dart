
abstract class LoginEvent {
}

class LoginInWithNikButtonPressed extends LoginEvent {
  final String nik;
  final String password;

  LoginInWithNikButtonPressed({required this.nik, required this.password});
}
