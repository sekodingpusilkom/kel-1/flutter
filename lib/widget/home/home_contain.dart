import 'package:flutter/material.dart';
import 'package:simas/widget/aktivitas/aktivitas_page.dart';
import 'package:simas/widget/presence/presence_page.dart';

import 'package:simas/widget/profile/profile_page.dart';
import 'package:simas/widget/vaksinasi/vaksinasi_page.dart';

import '../unavalable_page.dart';

Widget homeContainer() {
  final List<Map> itemData = [
    {'icon': Icons.schedule, 'text': 'KEHADIRAN', 'press': PresencePage()},
    {
      'icon': Icons.health_and_safety_outlined,
      'text': 'KESEHATAN',
      'press': UnavailablePage(),
    },
    {
      'icon': Icons.assignment_turned_in,
      'text': 'VAKSINASI',
      'press': VaksinasiPage(),
    },
    {
      'icon': Icons.hiking,
      'text': 'AKTIVITAS',
      'press': AktivitasPage(),
    },
    {'icon': Icons.person, 'text': 'PROFIL', 'press': ProfilePage()},
  ];
  return Center(
    child: GridView.builder(
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 150,
        childAspectRatio: 1,
        crossAxisSpacing: 2,
        mainAxisSpacing: 1,
      ),
      itemCount: itemData.length,
      itemBuilder: (BuildContext ctx, index) {
        return buildGridMenuItem(
          ctx,
          itemData[index]['icon'],
          itemData[index]['text'],
          itemData[index]['press'],
        );
      },
    ),
  );
}

Widget buildGridMenuItem(
    BuildContext context, IconData theIcon, String theLabel, destinationRoute) {
  var deviceData = MediaQuery.of(context);
  return Scaffold(
    body: GridTile(
      child: Center(
        child: Card(
          margin: EdgeInsets.all(deviceData.size.width / 20),
          shape: RoundedRectangleBorder(
              borderRadius:
                  BorderRadius.circular(deviceData.textScaleFactor * 20)),
          elevation: 5,
          child: InkWell(
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                ),
                Expanded(
                    child: Center(
                  child: SizedBox(
                    height: deviceData.textScaleFactor * 60,
                    width: deviceData.textScaleFactor * 60,
                    // width: (deviceData.size.width / 12),
                    child: Icon(
                      theIcon,
                      size: deviceData.textScaleFactor * 60,
                      color: Colors.white,
                    ),
                  ),
                ),
                ),
                Center(
                  child: Text(
                    theLabel,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: deviceData.textScaleFactor * 12,
                    ),
                  ),
                ),
              ],
            ),
            onTap: () {
              // additionalStatement;
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => destinationRoute),
              );
            },
          ),
        ),
      ),
    ),
  );
}
