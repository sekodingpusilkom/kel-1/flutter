class ClockinProvider {
  TheNavigation theNavigation;

  ClockinProvider({required this.theNavigation});

  TheNavigation getNavigation() {
    return theNavigation;
  }
}

enum TheNavigation { satu, dua, tiga }
