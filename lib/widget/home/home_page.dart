// ignore_for_file: unused_field

import 'package:flutter/material.dart';
import 'package:simas/widget/aktivitas/aktivitas_page.dart';
import 'package:simas/widget/general/appbar_widget.dart';
import 'package:simas/widget/history/history_page.dart';
import 'package:simas/widget/profile/profile_contain.dart';

import '../unavalable_contain.dart';
import 'home_contain.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key}) : super(key: key); // ini blm ada
  // final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedBar = 2;

  static List<Widget> _widgetOptions = <Widget>[
    UnavailableContain(),
    UnavailableContain(),
    homeContainer(),
    UnavailableContain(),
    ProfileContain(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedBar = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> _widgetOptions = <Widget>[
      AktivitasPage(),
      HistoryPage(),
      homeContainer(),
      // MentalContain(),
      // RiwayatVaksinasiPage(),
      UnavailableContain(),
      ProfileContain(),
    ];

    return Scaffold(
        key: Key("homeMenu"),
        appBar: headerNav(),
        body: _widgetOptions.elementAt(_selectedBar),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.hiking),
              label: 'Aktivitas',
              tooltip: 'Feed Aktivitas',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.history),
              label: 'Riwayat',
              tooltip: 'Riwayat Kehadiran',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Beranda',
              tooltip: 'Beranda',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.medical_services_outlined),
              label: 'Ijin / Sakit',
              tooltip: 'Riwayat Kesehatan',
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.person), label: 'Profil', tooltip: 'Profil,'),
          ],
          currentIndex: _selectedBar,
          showUnselectedLabels: true,
          onTap: _onItemTapped,
        ),
    );
  }
}
