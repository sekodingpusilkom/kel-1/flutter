// ignore_for_file: unnecessary_null_comparison
import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simas/widget/aktivitas_upload/aktivitas_upl_bloc.dart';
import 'package:simas/widget/aktivitas_upload/aktivitas_upl_event.dart';
import 'package:simas/widget/aktivitas_upload/aktivitas_upl_state.dart';
import 'package:simas/models/commons/method_common.dart';
import 'package:image_picker/image_picker.dart';
import 'package:simas/widget/presence/camera.dart';

class AktivitasUplContain extends StatefulWidget {
  const AktivitasUplContain({
    Key? key,
  }) : super(key: key);

  @override
  _AktivitasUplContainState createState() => _AktivitasUplContainState();
}

class _AktivitasUplContainState extends State<AktivitasUplContain> {
  bool imgFile = false;
  String imgPath = '';
  late SharedPreferences prefs;
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  final _descController = TextEditingController();

  Future<void> pressCamera(BuildContext context) async {
    WidgetsFlutterBinding.ensureInitialized();
    final cameras = await availableCameras();
    final firstCamera = cameras[1];
    imgPath = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => TakePictureScreen(
          camera: firstCamera,
        ),
      ),
    );
    // print(imgPath);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final _aktivitasUplBloc = BlocProvider.of<AktivitasUplBloc>(context);

    Future<void> _handleUpload(BuildContext context) async {
      prefs = await SharedPreferences.getInstance();
      if (_key.currentState!.validate()) {
        if (imgPath == "") {
          showDialogBox(context, "Mohon ambil foto aktivitas kebugaran.");
        } else {
          _aktivitasUplBloc.add(
              AktivitasUplButtonPressed(prefs.getString("userId"), imgPath, _descController.text));
        }
      }
    }
    // bloc.add(namaEvent);
    renderUploadButton() {
      return ElevatedButton(
        key: Key("upload"),
        child: Text(
          'Upload',
        ),
        style: ElevatedButton.styleFrom(
            primary: Color.fromRGBO(250, 64, 113, 1),
            minimumSize: Size(300.0, 45.0),
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(5.0))),
        onPressed: () async {
          await _handleUpload(context);
        },
      );
    }

    void _openGallery(BuildContext context) async {
      ImagePicker imgPicker = new ImagePicker();
      var picture = await imgPicker.pickImage(source: ImageSource.gallery);
      imgPath = picture?.path ?? "";
      setState(() {});
    }

    Future<void> chooseMedia(BuildContext context) {
      return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
                title: Text("Pilih sumber"),
                content: SingleChildScrollView(
                  child: ListBody(
                    children: <Widget>[
                      GestureDetector(
                        child: Text("Galeri"),
                        onTap: () {
                          _openGallery(context);
                          Navigator.of(context).pop();
                        },
                      ),
                      Padding(padding: EdgeInsets.all(8.0)),
                      GestureDetector(
                        child: Text("Camera"),
                        onTap: () {
                          pressCamera(context);
                          Navigator.of(context).pop();
                        },
                      )
                    ],
                  ),
                ));
          });
    }

    OutlinedButton renderChooseButton(BuildContext context) {
      return OutlinedButton(
          key: Key("choose"),
          onPressed: () async {
            await chooseMedia(context);
          },
          child: Stack(
            children: <Widget>[
              Align(
                  alignment: Alignment.centerRight,
                  child: Icon(Icons.photo_camera,
                      color: Color.fromRGBO(250, 64, 113, 1))),
              Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "Ambil gambar",
                    textAlign: TextAlign.left,
                  ))
            ],
          ),
          style: OutlinedButton.styleFrom(
              primary: Colors.black,
              minimumSize: Size(400.0, 55.0),
              shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(4.0)),
              side: BorderSide(color: Color.fromRGBO(250, 64, 113, 1))));
    }

    return BlocListener<AktivitasUplBloc, AktivitasUplState>(
        listener: (context, state) async {
      if (state is AktivitasUplSuccess) {
        showDialogBox(context, 'Upload aktivitas kebugaran berhasil');
        _descController.text = "";
        imgPath = "";
      } else if (state is AktivitasUplFailure) {
        showDialogBox(context, state.error);
      }
    }, child: BlocBuilder<AktivitasUplBloc, AktivitasUplState>(
            builder: (context, state) {
      return Form(
          key: _key,
          child: Container(
              padding: EdgeInsets.all(50.0),
              child: BlocBuilder<AktivitasUplBloc, AktivitasUplState>(
                  builder: (context, state) {
                if (state is AktivitasUplLoading) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                return Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 20.0),
                    ),
                    (imgPath != null && imgPath.isNotEmpty)
                        ? Image.file(File(imgPath),
                            height: MediaQuery.of(context).size.height / 3)
                        : renderChooseButton(context),
                    Padding(
                      padding: EdgeInsets.only(top: 20.0),
                    ),
                    (imgPath != null && imgPath.isNotEmpty)
                        ? renderChooseButton(context)
                        : Padding(
                            padding: EdgeInsets.only(top: 10.0),
                          ),
                    Padding(
                      padding: EdgeInsets.only(top: 10.0),
                    ),
                    TextFormField(
                      key: Key("description"),
                      decoration: InputDecoration(
                        labelText: 'Deskripsi',
                        filled: true,
                        isDense: true,
                      ),
                      controller: _descController,
                      autocorrect: false,
                      validator: (value) {
                        if (value == null || value == "") {
                          return 'Deskripsi diperlukan.';
                        }
                        return null;
                      },
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 30.0),
                    ),
                    renderUploadButton(),
                  ],
                );
              })));
    }));
  }
}
