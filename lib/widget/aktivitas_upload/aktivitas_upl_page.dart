import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simas/widget/aktivitas_upload/aktivitas_upl_bloc.dart';
import 'package:simas/widget/aktivitas_upload/aktivitas_upl_contain.dart';
import 'package:simas/widget/general/appbar_widget.dart';

class AktivitasUplPage extends StatelessWidget {
  Widget build(context) {
    return Scaffold(
        appBar: headerNav(),
        body: BlocProvider<AktivitasUplBloc>(
          create: (context) => AktivitasUplBloc(),
          child:SingleChildScrollView(
            child: AktivitasUplContain()
          ),
        )
      // bottomNavigationBar: theBottomNav(),
    );
  }
}
