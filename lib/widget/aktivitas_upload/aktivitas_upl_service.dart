import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';
import 'package:simas/models/commons/exception_constant.dart';
import 'package:simas/models/commons/url_constant.dart' as ConstantUrl;
import 'package:simas/widget/multipartreq_provider.dart';

abstract class AktivitasUplService {
  Future<void> submitPost(String? userId, String imagePath,
      String description, String? token);
}

class AktivitasUplServiceImpl extends AktivitasUplService {
  Client client = new Client();

  @override
  Future<void> submitPost(String? userId, String imagePath,
      String description, String? token) async {

    if(userId == "" || imagePath == "" || description == ""){
      throw Exception(ExceptionConstant.AKT_UPL_FAILED);
    }

    try {
      final request =
      new MultipartRequest("POST", Uri.parse(ConstantUrl.EXERCISE));
      request.headers['authorization'] = "Bearer $token";
      request.headers['content-type'] = "multipart/form-data";
      request.fields['users_id'] = userId ?? "";
      request.fields['description'] = description;
      request.files.add(await MultipartFile.fromPath(
        'photo',
        imagePath,
        contentType: new MediaType('image', 'jpg'),
      ));

      var reqObj = GetIt.instance.get<BaseMultiPartReq>();
      var response = await reqObj.sendRequest(request);

      if (response.statusCode != 200) {
        throw Exception(ExceptionConstant.AKT_UPL_FAILED);
      }
    }
    catch(err){
      throw Exception(ExceptionConstant.AKT_UPL_FAILED);
    }
  }
}
