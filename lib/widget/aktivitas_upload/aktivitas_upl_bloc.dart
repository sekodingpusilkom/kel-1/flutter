import 'package:http/http.dart' show Client;
import 'package:bloc/bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simas/models/commons/exception_constant.dart';
import 'package:simas/widget/aktivitas_upload/aktivitas_upl_event.dart';
import 'package:simas/widget/aktivitas_upload/aktivitas_upl_service.dart';
import 'package:simas/widget/aktivitas_upload/aktivitas_upl_state.dart';

class AktivitasUplBloc extends Bloc<AktivitasUplEvent, AktivitasUplState>{
  AktivitasUplService _aktivitasUplService = new AktivitasUplServiceImpl();
  AktivitasUplBloc()
      : super(AktivitasUplInitial());

  Client client = Client();

  @override
  Stream<AktivitasUplState> mapEventToState(AktivitasUplEvent event) async* {
    if (event is AktivitasUplButtonPressed) {
      yield* _mapLoginToState(event);
    }
  }
  Stream<AktivitasUplState> _mapLoginToState(AktivitasUplButtonPressed event) async* {
    yield AktivitasUplLoading();
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await _aktivitasUplService.submitPost(event.userId, event.imagePath, event.description, prefs.getString("token"));
      yield AktivitasUplSuccess();
    }
    catch (err) {
      yield AktivitasUplFailure(ExceptionConstant.AKT_UPL_FAILED);
    }

  }

}