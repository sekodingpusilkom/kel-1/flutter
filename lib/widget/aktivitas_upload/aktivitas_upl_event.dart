
abstract class AktivitasUplEvent {
}

class AktivitasUplButtonPressed extends AktivitasUplEvent {
  final String? userId;
  final String description;
  final String imagePath;

  AktivitasUplButtonPressed(this.userId, this.imagePath, this.description);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AktivitasUplButtonPressed &&
          runtimeType == other.runtimeType &&
          userId == other.userId &&
          description == other.description &&
          imagePath == other.imagePath;

  @override
  int get hashCode =>
      userId.hashCode ^ description.hashCode ^ imagePath.hashCode;
}

