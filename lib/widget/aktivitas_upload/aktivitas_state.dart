import 'package:simas/models/aktivitas_model.dart';

abstract class AktivitasState {
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is AktivitasState && runtimeType == other.runtimeType;

  @override
  int get hashCode => 0;
}

class AktivitasInitial extends AktivitasState {
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          super == other &&
              other is AktivitasInitial &&
              runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode;
}

class AktivitasLoading extends AktivitasState {
  AktivitasLoading();
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          super == other &&
              other is AktivitasLoading &&
              runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode;
}

class AktivitasShareSuccess extends AktivitasState {
  final String filePath;
  final String desc;
  AktivitasShareSuccess(this.filePath, this.desc);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          super == other &&
              other is AktivitasShareSuccess &&
              runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode;
}

class AktivitasGetDataSuccess extends AktivitasState {
  final List<AktivitasModel> aktivitasModels;

  AktivitasGetDataSuccess(this.aktivitasModels);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      super == other &&
          other is AktivitasGetDataSuccess &&
          runtimeType == other.runtimeType &&
          aktivitasModels == other.aktivitasModels;

  @override
  int get hashCode => super.hashCode ^ aktivitasModels.hashCode;
}

class AktivitasEditDeleteSuccess extends AktivitasState {
  final String message;

  AktivitasEditDeleteSuccess(this.message);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      super == other &&
          other is AktivitasEditDeleteSuccess &&
          runtimeType == other.runtimeType &&
          message == other.message;

  @override
  int get hashCode => super.hashCode ^ message.hashCode;
}


class AktivitasFailure extends AktivitasState {
  final String error;

  AktivitasFailure(this.error);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          super == other &&
              other is AktivitasFailure &&
              runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode ^ error.hashCode;

}
