abstract class AktivitasUplState {
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is AktivitasUplState && runtimeType == other.runtimeType;

  @override
  int get hashCode => 0;
}

class AktivitasUplInitial extends AktivitasUplState {
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          super == other &&
              other is AktivitasUplInitial &&
              runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode;
}

class AktivitasUplLoading extends AktivitasUplState {
  AktivitasUplLoading();
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          super == other &&
              other is AktivitasUplLoading &&
              runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode;
}

class AktivitasUplSuccess extends AktivitasUplState {
  AktivitasUplSuccess();

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          super == other &&
              other is AktivitasUplSuccess &&
              runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode;
}

class AktivitasUplFailure extends AktivitasUplState {
  final String error;

  AktivitasUplFailure(this.error);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          super == other &&
              other is AktivitasUplFailure &&
              runtimeType == other.runtimeType &&
              error == other.error;

  @override
  int get hashCode => super.hashCode ^ error.hashCode;

}
