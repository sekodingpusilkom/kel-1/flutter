
import 'dart:convert';

import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:simas/models/commons/exception_constant.dart';
import 'package:simas/models/commons/url_constant.dart' as ConstantUrl;

abstract class RiwayatVaksinasiService {
  Future getRiwayatVaksinasi(String? userId, String? token);
}

class RiwayatVaksinasiServiceImpl extends RiwayatVaksinasiService {
  @override
  Future getRiwayatVaksinasi(String? userId, String? token) async{
    try{
      var client = GetIt.instance.get<Client>();
      var response = await client.get(Uri.parse(ConstantUrl.VACCINE + "/" + userId!),  headers: {
        "content-type": "application/json",
        "accept": "application/json",
        "authorization": "Bearer $token"
      });

      if(response.statusCode == 200){
        var data = json.decode(response.body);
        return data;
      }
      else if(response.statusCode == 403 || response.statusCode == 401){
        throw Exception(ExceptionConstant.SESSION_EXPIRED);
      }
      else throw Exception(ExceptionConstant.RIWAYAT_VAKSINASI_FAILED);
    }
    catch(e){
      throw Exception(ExceptionConstant.RIWAYAT_VAKSINASI_FAILED);
    }
  }
  
}