import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:simas/models/commons/exception_constant.dart';
import 'package:simas/widget/riwayat_vaksinasi/riwayat_vaksinasi_service.dart';
import 'package:simas/widget/sharedprefs_provider.dart';
part 'riwayat_vaksinasi_event.dart';
part 'riwayat_vaksinasi_state.dart';
class RiwayatVaksinasiBloc extends Bloc<RiwayatVaksinasiEvent, RiwayatVaksinasiState> {
  RiwayatVaksinasiService _vaksinasiService = new RiwayatVaksinasiServiceImpl();

  RiwayatVaksinasiBloc() : super(RiwayatVaksinasiInitial());

  @override
  Stream<RiwayatVaksinasiState> mapEventToState(
      RiwayatVaksinasiEvent event) async* {
    var localPrefs = GetIt.instance.get<BaseSharedPrefs>();
    String token = await localPrefs.getString('token');
    String userId = await localPrefs.getString('userId');

    if (event is RiwayatVaksinasiGetDataEvent) {
      yield* _mapGetData(event, token, userId);
    }
  }

  Stream<RiwayatVaksinasiState> _mapGetData(RiwayatVaksinasiGetDataEvent event,
      String token, String userId) async* {
    yield RiwayatVaksinasiLoading();
    try {
      var vaksinasiList = await _vaksinasiService.getRiwayatVaksinasi(
          userId, token);
      yield RiwayatVaksinasiSuccess(vaksinasiList);
    }
    catch (err) {
      // err.message
      // TODO: tambah session exp
      yield RiwayatVaksinasiFailure(ExceptionConstant.RIWAYAT_VAKSINASI_FAILED);
    }
  }
}