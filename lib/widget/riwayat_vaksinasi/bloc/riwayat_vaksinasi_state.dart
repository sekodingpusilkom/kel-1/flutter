part of 'riwayat_vaksinasi_bloc.dart';

abstract class RiwayatVaksinasiState {
  const RiwayatVaksinasiState();

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is RiwayatVaksinasiState && runtimeType == other.runtimeType;

  @override
  int get hashCode => 0;
}

class RiwayatVaksinasiInitial extends RiwayatVaksinasiState {
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          super == other &&
              other is RiwayatVaksinasiInitial &&
              runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode;
}

class RiwayatVaksinasiLoading extends RiwayatVaksinasiState {
  RiwayatVaksinasiLoading();

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          super == other &&
              other is RiwayatVaksinasiLoading &&
              runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode;
}

class RiwayatVaksinasiSuccess extends RiwayatVaksinasiState {
  var listVaksinasi = [];

  RiwayatVaksinasiSuccess(this.listVaksinasi);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      super == other &&
          other is RiwayatVaksinasiSuccess &&
          runtimeType == other.runtimeType &&
          listVaksinasi == other.listVaksinasi;

  @override
  int get hashCode => super.hashCode ^ listVaksinasi.hashCode;
}

class RiwayatVaksinasiFailure extends RiwayatVaksinasiState {
  final String error;

  RiwayatVaksinasiFailure(this.error);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          super == other &&
              other is RiwayatVaksinasiFailure &&
              runtimeType == other.runtimeType &&
              error == other.error;

  @override
  int get hashCode => super.hashCode ^ error.hashCode;

}
