// ignore_for_file: unnecessary_null_comparison
// import 'package:date_range_form_field/date_range_form_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:simas/models/commons/method_common.dart';
import 'package:simas/widget/riwayat_vaksinasi/bloc/riwayat_vaksinasi_bloc.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:simas/models/commons/url_constant.dart' as ConstantUrl;
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:shared_preferences/shared_preferences.dart';

class RiwayatVaksinasiContain extends StatefulWidget {
  @override
  _RiwayatVaksinasiContainState createState() =>
      _RiwayatVaksinasiContainState();
}

GlobalKey<FormState> myFormKey = new GlobalKey();

class _RiwayatVaksinasiContainState extends State<RiwayatVaksinasiContain> {
  var dataList = <Widget>[];
  _generateSingleStatus(
      String vaccineType, String number, String vaccineDate, String filePath) {
    return Card(
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
        child: Column(children: [
          Row(
            children: [
              Text("Jenis Vaksinasi"),
              Padding(padding: EdgeInsets.only(right: 10, top: 20, bottom: 20)),
              Text(vaccineType)
            ],
          ),
          Row(
            children: [
              Text("Jumlah Vaksinasi"),
              Padding(padding: EdgeInsets.only(right: 10, top: 20, bottom: 20)),
              Text(number)
            ],
          ),
          Row(
            children: [
              Text("Tanggal Vaksinasi"),
              Padding(padding: EdgeInsets.only(right: 10, top: 20, bottom: 20)),
              Text(vaccineDate)
            ],
          ),
          Row(
            children: [
              Text('Download sertifikat'),
              Padding(padding: EdgeInsets.only(right: 10, top: 20, bottom: 20)),
              InkWell(
                  child: new Icon(Icons.download, color: Colors.blue.shade400),
                  onTap: () => launch(filePath))
              // Image.network(
              //     filePath,
              //     fit: BoxFit.contain,
              //     width: 300, errorBuilder: (BuildContext context,
              //     Object exception, StackTrace? stackTrace) {
              //   return SizedBox(
              //       height: 200.0,
              //       width: 100.0,
              //       child: Icon(Icons.error, size: 60.0));
              // })
            ],
          )
        ]),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RiwayatVaksinasiBloc, RiwayatVaksinasiState>(
        listener: (context, state) {
      if (state is RiwayatVaksinasiFailure) {
        showDialogBox(context, state.error);
      }
    }, child: BlocBuilder<RiwayatVaksinasiBloc, RiwayatVaksinasiState>(
            builder: (context, state) {
      if (state is RiwayatVaksinasiLoading) {
        return Center(
          child: CircularProgressIndicator(),
        );
      } else if (state is RiwayatVaksinasiSuccess) {
        var dataVaksinasi = state.listVaksinasi;
        dataList = [];
        for (var i = 0; i < dataVaksinasi.length; i++) {
          String certif =
              ConstantUrl.FILE_VACCINE_URL + dataVaksinasi[i]['certificate'];
          String tempDate = dataVaksinasi[i]['vaccineDate'];
          String date = "";
          if (tempDate != null && tempDate != "") {
            date = DateFormat("yyyy-MM-dd")
                .format(DateTime.parse(tempDate))
                .toString();
          } else {
            date = "-";
          }
          var singleStatus = _generateSingleStatus(
              dataVaksinasi[i]['vaccine_name'],
              dataVaksinasi[i]['number'].toString(),
              date,
              certif);
          dataList.add(singleStatus);
        }
      }

      return Scaffold(
          body: SafeArea(
              child: SingleChildScrollView(
        child: Container(
            padding: EdgeInsets.all(10.0),
            child: Column(
                children:
                    // _generateSingleStatus("vacc", "1", "2021-12",
                    //     'http://35.212.240.171/file/resources/vaccination/1628825082636.pdf')
                    dataList.length == 0
                        ? [
                            Center(
                                child:
                                    Text('Belum ada Riwayat Vaksinasi Terkini'))
                          ]
                        : dataList)),
      )));
    }));
  }
}
