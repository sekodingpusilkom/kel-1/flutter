// ignore_for_file: unused_field
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:simas/repository/repository_history.dart';
import 'package:simas/widget/general/appbar_widget.dart';
import 'package:simas/widget/riwayat_vaksinasi/bloc/riwayat_vaksinasi_bloc.dart';
// import 'package:simas/widget/general/appbar_widget.dart';

import 'riwayat_vaksinasi_contain_with_data.dart';

class RiwayatVaksinasiPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<RiwayatVaksinasiBloc>(
        create: (context) => RiwayatVaksinasiBloc(), child: _RiwayatPage());
  }

}

class _RiwayatPage extends StatefulWidget {
  @override
  __RiwayatVaksinasiPageState createState() => __RiwayatVaksinasiPageState();
}

class __RiwayatVaksinasiPageState extends State<_RiwayatPage> {
  late RiwayatVaksinasiBloc _bloc;
  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of<RiwayatVaksinasiBloc>(context)..add(RiwayatVaksinasiGetDataEvent());
  }

  Widget build(context) {
    return  Scaffold(
          appBar: headerNav(),
          body: RiwayatVaksinasiContain(),
    );
  }
}
