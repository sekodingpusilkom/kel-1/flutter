import 'package:flutter/material.dart';

class ButtonProfil extends StatelessWidget {
  const ButtonProfil({
    Key? key,
    required this.menu,
    required this.icon,
    required this.press,
  }) : super(key: key);

  final String menu;
  final Icon icon;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(5, 10, 5, 5),
      child: TextButton(
        key: key,
        onPressed: press,
        style: TextButton.styleFrom(
          padding: const EdgeInsets.all(16.0),
          primary: Colors.pink.shade400,
          textStyle: const TextStyle(fontSize: 20),
        ),
        child: Row(
          children: [
            icon,
            SizedBox(
              width: 25,
            ),
            Expanded(
              child: Text(
                menu,
                // style: TextStyle(color: Colors.black54),
              ),
            ),
            Icon(Icons.arrow_forward_ios)
          ],
        ),
      ),
    );
  }
}
