abstract class PresenceState {
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PresenceState && runtimeType == other.runtimeType;

  @override
  int get hashCode => 0;
}

class PresenceInitial extends PresenceState {
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      super == other &&
          other is PresenceInitial &&
          runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode;
}

class PresenceLoading extends PresenceState {
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      super == other &&
          other is PresenceLoading &&
          runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode;
}

class PresenceSuccess extends PresenceState {
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      super == other &&
          other is PresenceSuccess &&
          runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode;
}

class PresenceFailure extends PresenceState {
  final String error;

  PresenceFailure(this.error);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      super == other &&
          other is PresenceFailure &&
          runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode;
}
