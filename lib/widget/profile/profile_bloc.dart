import 'package:bloc/bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simas/widget/presence/presence_service.dart';
import 'package:simas/widget/presence/presence_state.dart';
import 'package:simas/widget/presence/presence_event.dart';
import 'package:simas/widget/presence/presence_exception.dart';

class PresenceBloc extends Bloc<PresenceEvent, PresenceState> {
  PresenceService _presenceService = new FakePresenceService();
  PresenceBloc() : super(PresenceInitial());

  @override
  Stream<PresenceState> mapEventToState(PresenceEvent event) async* {
    // _PresenceService = new FakePresenceService();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (event is CheckPhysical) {
      bool isCheckin = await _presenceService.isCheckIn(
        prefs.getString("userId"),
        prefs.getString("token"),
      );
      prefs.setBool("isCheckin", isCheckin);
    } else if (event is CheckMental) {
      bool isFillin = await _presenceService.isFillIn(
        prefs.getString("userId"),
        prefs.getString("token"),
      );
      prefs.setBool("isFillin", isFillin);
    } else if (event is PresenceWithButtonPressed) {
      yield PresenceLoading();
      try {
        String dateResult = await _presenceService.submitPresence(
          prefs.getString("userId"),
          event.imagePath,
          event.latitude,
          event.longitude,
          prefs.getString("token"),
          event.mental,
          event.theUrl,
        );
        // prefs.setString("presence", dateResult);
        // print('Response dateresult : $dateResult');
        if (dateResult.isNotEmpty && dateResult != "") {
          yield PresenceSuccess();
          yield PresenceInitial();
        } else {
          yield PresenceFailure('Something very weird just happened');
        }
      } on PresenceException catch (e) {
        yield PresenceFailure(e.message);
      } catch (err) {
        yield PresenceFailure('An unknown error occured');
      }
    }
  }
}
