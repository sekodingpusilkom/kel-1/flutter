import 'package:flutter/material.dart';
import 'package:simas/widget/general/appbar_widget.dart';

import 'profile_contain.dart';

class ProfilePage extends StatelessWidget {
  Widget build(context) {
    return Scaffold(
      appBar: headerNav(),
      body: ProfileContain(),
      // bottomNavigationBar: theBottomNav(),
    );
  }
}
