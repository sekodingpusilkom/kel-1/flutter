import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simas/widget/authentication/authentication_bloc.dart';
import 'package:simas/widget/authentication/authentication_event.dart';
import 'package:simas/widget/change_password/change_password_page.dart';
import 'package:simas/models/commons/url_constant.dart' as ConstantUrl;
import 'package:simas/widget/sharedprefs_provider.dart';
import 'button_profile.dart';

class ProfileContain extends StatefulWidget {
  const ProfileContain({
    Key? key,
  }) : super(key: key);

  @override
  State<ProfileContain> createState() => _ProfileContainState();
}

class _ProfileContainState extends State<ProfileContain> {
  late SharedPreferences preferences;

  late Future<LoginData> dataLogin;

  @override
  void initState() {
    super.initState();
    dataLogin = getLoginId();
  }

  Future<LoginData> getLoginId() async {
    preferences = await SharedPreferences.getInstance();
    var localPrefs = GetIt.instance.get<BaseSharedPrefs>();

    String id = await localPrefs.getString('userId');
    String tkn = await localPrefs.getString('token');
    // print("ini id $id");
    // print("ini tkn $tkn");
    return LoginData(userId: id, token: tkn);
  }

  Future<DataProfil> requestDetailUser(String? userId, String? token) async {
    Client client = GetIt.instance.get<Client>();
    // var client = Client();
    var response = await client.get(Uri.parse(ConstantUrl.USER_DETAIL + "/$userId"),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
          "authorization": "Bearer $token"
        }
    );

    // print("isi response status code : ${response.statusCode}");
    // print(resDecode.body);
    if (response.statusCode == 200) {
      var requestDetailUser = json.decode(response.body);
      // print("Is check in requestDetailUser : $requestDetailUser");
      return DataProfil(imgPath: '', fullName: requestDetailUser['name']);
    } else {
      // print('Else requestDetailUser');
      return DataProfil(imgPath: '', fullName: 'empty');
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<LoginData>(
      future: dataLogin,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return SafeArea(
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.all(5),
                ),
                SizedBox(
                  height: 120,
                  width: 120,
                  child: CircleAvatar(
                    backgroundColor: Colors.pink.shade400,
                    foregroundColor: Colors.white,
                    child: Icon(
                      Icons.person,
                      size: 96,
                    ),
                  ),
                ),
                Card(
                  // shape: ,
                  margin: EdgeInsets.fromLTRB(0, 3, 0, 0),
                  clipBehavior: Clip.antiAlias,
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: FutureBuilder<DataProfil>(
                      future: requestDetailUser(
                          snapshot.data!.userId, snapshot.data!.token),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return Text(
                            "${snapshot.data!.fullName}".toUpperCase(),
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.black87,
                            ),
                          );
                        }
                        return CircularProgressIndicator();
                      },
                    ),
                  ),
                  color: Colors.grey.shade100,
                  elevation: 5,
                ),
                ButtonProfil(
                  icon: Icon(Icons.lock),
                  menu: "Ganti password",
                  press: () async {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ChangePasswordPage()));
                  },
                ),
                ButtonProfil(
                  icon: Icon(Icons.link),
                  menu: "Link to Strava",
                  press: () {
                    _showDialog(context);
                  },
                ),
                ButtonProfil(
                  key: Key("Tombol Keluar"),
                  icon: Icon(Icons.logout),
                  menu: "Keluar",
                  press: () async {
                    await preferences.clear();
                    final authBloc =
                        BlocProvider.of<AuthenticationBloc>(context);
                    authBloc.add(UserLoggedOut());
                  },
                ),
              ],
            ),
          );
        }
        return CircularProgressIndicator();
      },
    );
  }

  void _showDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Info"),
          content: Text("Halaman yang Anda tuju belum tersedia"),
          actions: <Widget>[
            ElevatedButton(
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateProperty.all(Colors.pink.shade400),
              ),
              child: Text("OK"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

class DataProfil {
  String? imgPath;
  String? fullName;

  DataProfil({
    this.imgPath,
    this.fullName,
  });
}

class LoginData {
  String? token;
  String? userId;

  LoginData({
    this.userId,
    this.token,
  });
}
