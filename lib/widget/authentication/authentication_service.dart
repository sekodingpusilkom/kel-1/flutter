import 'dart:convert';

import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simas/models/user.dart';
import 'package:simas/widget/authentication/authentication_exception.dart';
import 'package:simas/models/commons/url_constant.dart' as ConstantUrl;

abstract class AuthenticationService {
  Future<User?> getCurrentUser();
  Future<User> signInWithEmailAndPassword(String nik, String password);
}

class AuthenticationServiceImpl extends AuthenticationService {
  Client client = new Client();
  @override
  Future<User?> getCurrentUser() async {
    return null; // return null for now
  }

  @override
  Future<User> signInWithEmailAndPassword(String nik, String password) async {
    List<String> feedback = await login(nik, password);
    if (feedback.length == 0) {
      throw AuthenticationException(
          message: 'NIK atau password yang anda masukkan salah');
    } else {
      String token = feedback[1];
      String userId = feedback[0];
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("token", token);
      prefs.setString("userId", userId);
      prefs.setBool("isFillin", false);
      prefs.setBool("isCheckin", false);
    }
    return User(nik, 'Test User');
  }

  Future<List<String>> login(String nik, String password) async {
    Client client = GetIt.instance.get<Client>();
    try {
      final response = await client.post(Uri.parse(ConstantUrl.LOGIN),
          headers: {
            "content-type": "application/json",
            "accept": "application/json",
          },
          body: json.encode({'nik': nik, 'password': password}));
      final responseDec = json.decode(response.body);
      if (responseDec.containsKey("status") && responseDec["status"] == 401) {
        return [];
      } else {
        return [responseDec['user_id'], responseDec['token']];
      }
    } catch (e) {
      print(e);
    }
    return [];
  }
}
