class AuthenticationException implements Exception{
  final String message;

  AuthenticationException({this.message = 'auth exception: Unknown error occurred. '});
}