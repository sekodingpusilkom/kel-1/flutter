import 'package:simas/models/user.dart';

abstract class AuthenticationEvent {
  AuthenticationEvent();

}

class AppLoaded extends AuthenticationEvent {}

class UserLoggedIn extends AuthenticationEvent{
  final User user;
  UserLoggedIn({required this.user});

}

class UserLoggedOut extends AuthenticationEvent{}