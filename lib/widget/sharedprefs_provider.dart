import 'package:shared_preferences/shared_preferences.dart';

abstract class BaseSharedPrefs{
  Future<String> getString(String key);
}

class SharedPrefsImpl implements BaseSharedPrefs{
  @override
  Future<String> getString(String key) async {
    SharedPreferences prefs;
    prefs = await SharedPreferences.getInstance();
    return prefs.getString(key) ?? "";
  }
}