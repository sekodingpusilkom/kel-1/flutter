import 'dart:convert';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';
import 'package:simas/models/commons/url_constant.dart' as ConstantUrl;
import 'package:simas/widget/multipartreq_provider.dart';

abstract class PresenceService {
  Future<String> submitPresence(
      String? userId,
      String imagePath,
      double latitude,
      double longitude,
      String? token,
      List<int> mental,
      int theUrl);
  Future<bool> isCheckIn(String? userId, String? token);
  Future<bool> isFillIn(String? userId, String? token);
}

class FakePresenceService extends PresenceService {
  Client client = new Client();

  @override
  Future<String> submitPresence(
      String? userId,
      String imagePath,
      double latitude,
      double longitude,
      String? token,
      List<int> mental,
      int theUrl) async {
    String savedTime = "";
    List<String> moment = ['start', 'end'];
    final request =
        new MultipartRequest("POST", Uri.parse(ConstantUrl.PRESENCE[theUrl]));
    print(ConstantUrl.PRESENCE[theUrl]);
    // print('lat_${moment[theUrl]}');

    request.headers['authorization'] = "Bearer $token";
    request.headers['content-type'] = "multipart/form-data";
    request.fields['users_id'] = userId!;
    request.fields['lat_${moment[theUrl]}'] = latitude.toString();
    request.fields['long_${moment[theUrl]}'] = longitude.toString();
    request.fields['mental'] = mental[0].toString();
    request.fields['physical'] = mental[1].toString();
    request.files.add(await MultipartFile.fromPath(
      'photo_${moment[theUrl]}',
      imagePath,
      contentType: new MediaType('image', 'jpg'),
    ));

    // var response = await request.send();

    var reqObj = GetIt.instance.get<BaseMultiPartReq>();
    var response = await reqObj.sendRequest(request);

    if(response is Response && response.statusCode == 200){
      return "11:11:11";
    }
    print('Kiriman FIle : ${request.fields}');
    var resDecode = await Response.fromStream(response);
    var test = json.decode(resDecode.body);
    print('sResponse body : $test');
    print('Response code : ${response.statusCode}');
    if (response.statusCode == 200) {
      DateTime savedTime = DateTime.parse(test['time']);
      return '${savedTime.hour}:${savedTime.minute}:${savedTime.second}';
    } else if (response.statusCode == 400) {
      DateTime savedTime = DateTime.parse(test['error']);
      return '${savedTime.hour}:${savedTime.minute}:${savedTime.second}';
    } else {
      print('Else Submit');
      return savedTime;
    }
  }

  Future<bool> isCheckIn(String? userId, String? token) async {
    final request = new MultipartRequest(
        "GET", Uri.parse(ConstantUrl.IS_CLOCK_IN + "/$userId"));
    request.headers['authorization'] = "Bearer $token";

    var response = await request.send();
    print("isi response status code : ${response.statusCode}");
    var resDecode = await Response.fromStream(response);
    if (response.statusCode == 200) {
      var isCheckin = json.decode(resDecode.body);
      print("Is check in : $isCheckin");
      return isCheckin;
    } else {
      print('Else Checkin');
      return false;
    }
  }

  Future<bool> isFillIn(String? userId, String? token) async {
    final request = new MultipartRequest(
        "GET", Uri.parse(ConstantUrl.IS_FILL_MENTAL + "/$userId"));
    print(ConstantUrl.IS_FILL_MENTAL + "/$userId");
    request.headers['authorization'] = "Bearer $token";

    var response = await request.send();
    // print(response.statusCode);
    if (response.statusCode == 200) {
      var resDecode = await Response.fromStream(response);
      var isFillIn = json.decode(resDecode.body);
      print("Is fill in : ${isFillIn['isFilled']}");
      return isFillIn['isFilled'];
    } else {
      print('Else Mental');
      return false;
    }
  }
}
