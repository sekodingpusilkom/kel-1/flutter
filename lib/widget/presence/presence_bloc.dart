import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simas/models/commons/exception_constant.dart';
import 'package:simas/widget/presence/presence_service.dart';
import 'package:simas/widget/presence/presence_state.dart';
import 'package:simas/widget/presence/presence_event.dart';
import 'package:simas/widget/presence/presence_exception.dart';
import 'package:simas/widget/sharedprefs_provider.dart';

class PresenceBloc extends Bloc<PresenceEvent, PresenceState> {
  PresenceService _presenceService = new FakePresenceService();
  PresenceBloc() : super(PresenceInitial());

  @override
  Stream<PresenceState> mapEventToState(PresenceEvent event) async* {
    // _PresenceService = new FakePresenceService();

    SharedPreferences prefs = await SharedPreferences.getInstance();

    var localPrefs = GetIt.instance.get<BaseSharedPrefs>();
    String userId = await localPrefs.getString('userId');
    String token = await localPrefs.getString('token');

    if (event is CheckPhysical) {
      bool isCheckin = await _presenceService.isCheckIn(
        userId,
        token,
      );
      prefs.setBool("isCheckin", isCheckin);
    } else if (event is CheckMental) {
      bool isFillin = await _presenceService.isFillIn(
        userId,
        token,
      );
      prefs.setBool("isFillin", isFillin);
    } else if (event is PresenceWithButtonPressed) {
      yield PresenceLoading();
      try {
        String dateResult = await _presenceService.submitPresence(
          userId,
          event.imagePath,
          event.latitude,
          event.longitude,
          token,
          event.mental,
          event.theUrl,
        );
        // prefs.setString("presence", dateResult);
        // print('Response dateresult : $dateResult');
        if (dateResult.isNotEmpty && dateResult != "") {
          yield PresenceSuccess();
          yield PresenceInitial();
        } else {
          yield PresenceFailure(ExceptionConstant.PRES_UPL_FAILED);
        }
      } on PresenceException catch (e) {
        yield PresenceFailure(e.message);
      } catch (err) {
        yield PresenceFailure(ExceptionConstant.PRES_UPL_FAILED);
      }
    }
  }
}
