class PresenceException implements Exception {
  final String message;

  PresenceException({this.message = 'Unknown error occurred. '});
}
