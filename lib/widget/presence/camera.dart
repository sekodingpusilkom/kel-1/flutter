import 'dart:async';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:simas/widget/general/appbar_widget.dart';

// A screen that allows users to take a picture using a given camera.
class TakePictureScreen extends StatefulWidget {
  const TakePictureScreen({Key? key, required this.camera}) : super(key: key);

  final CameraDescription camera;

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  late CameraController _controller;
  late Future<void> _initializeControllerFuture;

  int slctdCamera = 1;

  @override
  void initState() {
    super.initState();

    _controller = CameraController(
      widget.camera,
      ResolutionPreset.low,
    );

    // Next, initialize the controller. This returns a Future.
    _initializeControllerFuture = _controller.initialize();
  }

  Future<void> selectCamera() async {
    WidgetsFlutterBinding.ensureInitialized();
    final cameras = await availableCameras();

    slctdCamera = (slctdCamera < 1) ? 1 : 0;
    final firstCamera = cameras[slctdCamera];
    _controller = CameraController(
      firstCamera,
      ResolutionPreset.low,
    );
    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: headerNav(),
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          return Container(
            child: SafeArea(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  (snapshot.connectionState == ConnectionState.done)
                      ? Container(
                          child: CameraPreview(_controller),
                          height: MediaQuery.of(context).size.height / 1.5,
                        )
                      : Container(
                          height: MediaQuery.of(context).size.height / 1.5,
                          child: Center(
                            child: CircularProgressIndicator(),
                          ),
                        ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(80),
                      child: ElevatedButton(
                        onPressed: () async {
                          try {
                            await _initializeControllerFuture;
                            final image = await _controller.takePicture();
                            // print(object)
                            Navigator.pop(context, image.path);
                          } catch (e) {
                            print(e);
                          }
                        },
                        style: ElevatedButton.styleFrom(
                            primary: Color.fromRGBO(250, 64, 113, 1),
                            minimumSize: Size(300.0, 45.0),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(5.0))),
                        child: Icon(
                          Icons.camera_alt,
                          size: MediaQuery.of(context).size.height / 25,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(40),
                      child: ElevatedButton(
                        onPressed: () async {
                          selectCamera();
                          setState(() {});
                        },
                        style: ElevatedButton.styleFrom(
                            primary: Color.fromRGBO(250, 64, 113, 1),
                            minimumSize: Size(300.0, 45.0),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(5.0))),
                        child: Icon(
                          Icons.switch_camera,
                          size: MediaQuery.of(context).size.height / 25,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
