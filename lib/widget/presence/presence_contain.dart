// ignore_for_file: import_of_legacy_library_into_null_safe
// ignore_for_file: unnecessary_null_comparison
import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:geocoder/geocoder.dart';
import 'package:simas/models/commons/method_common.dart';
import 'package:simas/widget/home/home_page.dart';
import 'package:simas/widget/login/gps_checker.dart';
import 'package:simas/widget/presence/presence_bloc.dart';
import 'package:simas/widget/presence/presence_event.dart';
import 'package:simas/widget/presence/presence_state.dart';

import 'camera.dart';

class PresenceContain extends StatefulWidget {
  const PresenceContain({
    Key? key,
  }) : super(key: key);

  @override
  _PresenceContainState createState() => _PresenceContainState();
}

class _PresenceContainState extends State<PresenceContain> {
  bool imgFile = false;
  String imgPath = '';
  dynamic currentPos;
  String locationText = "Lokasi";
  String buttonText = "";
  late String? userId;
  late Future<DataRequired> dataBool;
  late PresenceBloc _blocPresence;
  late int theUrl;

  var mentalValue;

  var physicalValue;

  double _currentSliderValue = 1;
  double _currentPhysicalValue = 1;
  double index = 0;
  String _currentLabel = '';
  String _currentPhysicalLabel = '';
  List<String> _mentalConditions = [
    '',
    '\u{1F628}',
    '\u{1F626}',
    '\u{1F60A}',
    '\u{1F606}',
    '\u{1F603}'
  ];

  List<String> _physicalConditions = [
    '',
    '\u{1F637}',
    '\u{1F635}',
    '\u{1F609}',
    '\u{1F60A}',
    '\u{1F604}',
  ];
  @override
  void initState() {
    super.initState();
    dataBool = fetchPrefs();
  }

  Future<DataRequired> fetchPrefs() async {
    _blocPresence = PresenceBloc();
    _blocPresence.add(CheckMental());
    _blocPresence.add(CheckPhysical());
    var prefs = await SharedPreferences.getInstance();
    userId = prefs.getString("userId");
    bool isFillin = prefs.getBool("isFillin")!;
    bool isClockin = prefs.getBool("isCheckin")!;
    return DataRequired(clockin: isClockin, fillmental: isFillin);
  }

  @override
  Widget build(BuildContext context) {
    final _presenceBloc = BlocProvider.of<PresenceBloc>(context);
    var _styleQue = TextStyle(fontWeight: FontWeight.bold);

    Future<DataFeedback> _handleClockin() async {
      _presenceBloc.add(PresenceWithButtonPressed(
          userId,
          imgPath,
          currentPos.latitude,
          currentPos.longitude,
          [
            _currentSliderValue.round().toInt(),
            _currentPhysicalValue.round().toInt()
          ],
          theUrl));
      var prefs = await SharedPreferences.getInstance();
      String? time = prefs.getString("presence");
      return DataFeedback(time: time);
    }

    return BlocProvider(
      create: (context) => PresenceBloc(),
      child: BlocListener<PresenceBloc, PresenceState>(
        listener: (context, state) async {
          if (state is PresenceSuccess) {
            SharedPreferences prefs = await SharedPreferences.getInstance();
            showDialogBox(context,
                'Clock in berhasil pada pukul ${prefs.getString("presence")} tercatat pada waktu server');
          } else if (state is PresenceFailure) {
            showDialogBox(context,"Error");
          }
        },
        child: FutureBuilder<DataRequired>(
            future: dataBool,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Form(
                  child: Container(
                    padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                    child: BlocBuilder<PresenceBloc, PresenceState>(
                        builder: (context, state) {
                          if (state is PresenceLoading) {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                          return Column(
                            children: [
                              (imgPath != null && imgPath.isNotEmpty)
                                  ? Card(
                                color: Colors.transparent,
                                clipBehavior: Clip.antiAlias,
                                child: Column(
                                  children: [
                                    Image.file(File(imgPath),
                                        width: MediaQuery.of(context)
                                            .size
                                            .height /
                                            4),
                                    ElevatedButton(
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                        MainAxisAlignment.center,
                                        children: [
                                          Icon(Icons.camera_alt),
                                          Padding(
                                            padding: EdgeInsets.fromLTRB(
                                                5, 0, 5, 0),
                                          ),
                                          Text(
                                            'AMBIL ULANG',
                                          )
                                        ],
                                      ),
                                      style: ElevatedButton.styleFrom(
                                        primary:
                                        Color.fromRGBO(250, 64, 113, 1),
                                        minimumSize: Size(
                                            MediaQuery.of(context)
                                                .size
                                                .height /
                                                4.0,
                                            45.0),
                                      ),
                                      onPressed: () async {
                                        await pressCamera(context);
                                      },
                                    )
                                  ],
                                ),
                              )
                                  : cameraButton(context),
                              Padding(
                                padding: EdgeInsets.only(top: 15.0),
                              ),
                              OutlinedButton(
                                  onPressed: () async {
                                    setState(() {});
                                  },
                                  child: Stack(
                                    children: <Widget>[
                                      Align(
                                          alignment: Alignment.centerRight,
                                          child: Icon(Icons.location_on,
                                              color:
                                              Color.fromRGBO(250, 64, 113, 1))),
                                      Align(
                                          alignment: Alignment.centerLeft,
                                          child: FutureBuilder<DataLokasi>(
                                            future: getCurrentAddress(),
                                            builder: (context, snapshot) {
                                              if (snapshot.connectionState !=
                                                  ConnectionState.done) {
                                                return Center(
                                                  child: CircularProgressIndicator(
                                                    backgroundColor:
                                                    Colors.pink.shade400,
                                                    color: Colors.pink.shade100,
                                                  ),
                                                );
                                              }
                                              locationText =
                                                  snapshot.data!.lokasi.addressLine;
                                              return Text(
                                                locationText,
                                                textAlign: TextAlign.left,
                                              );
                                            },
                                          ))
                                    ],
                                  ),
                                  style: OutlinedButton.styleFrom(
                                      primary: Colors.black,
                                      minimumSize: Size(400.0, 55.0),
                                      shape: new RoundedRectangleBorder(
                                          borderRadius:
                                          new BorderRadius.circular(4.0)),
                                      side: BorderSide(
                                          color: Color.fromRGBO(250, 64, 113, 1)))),
                              Padding(
                                padding: EdgeInsets.only(top: 10.0),
                              ),
                              FutureBuilder<DataRequired>(
                                future: dataBool,
                                builder: (context, snapshot) {
                                  if (snapshot.hasData) {
                                    if (!snapshot.data!.fillmental) {
                                      return sliderMethod(_styleQue);
                                    }
                                  }
                                  return Padding(
                                    padding: EdgeInsets.only(top: 0.0),
                                  );
                                },
                              ),
                              ElevatedButton(
                                key: Key("submitPresence"),
                                child: FutureBuilder<DataRequired>(
                                    future: dataBool,
                                    builder: (context, snapshot) {
                                      if (snapshot.hasData) {
                                        if (snapshot.data!.clockin) {
                                          buttonText = "CLOCK OUT";
                                          theUrl = 1;
                                        } else {
                                          buttonText = "CLOCK IN";
                                          theUrl = 0;
                                        }
                                        return Text(buttonText);
                                      }
                                      return CircularProgressIndicator(
                                        color: Colors.white,
                                      );
                                    }),
                                style: ElevatedButton.styleFrom(
                                    primary: Color.fromRGBO(250, 64, 113, 1),
                                    minimumSize: Size(300.0, 45.0),
                                    shape: new RoundedRectangleBorder(
                                        borderRadius:
                                        new BorderRadius.circular(10.0))),
                                onPressed: () async {
                                  if (locationText == null) {
                                    return showDialogBox(context,"Mohon tekan tombol lokasi");
                                  }
                                  if (imgPath == "") {
                                    return showDialogBox(context,"Mohon ambil foto selfie");
                                  }
                                  FutureBuilder(
                                      future: _handleClockin(),
                                      builder: (context, snapshot) {
                                        return AlertDialog(
                                            content: Text("Berhasil"),
                                            actions: [
                                              ElevatedButton(
                                                child: Text('OK',
                                                    style: TextStyle(
                                                        color: Colors.white)),
                                                style: ElevatedButton.styleFrom(
                                                    primary: Color.fromRGBO(
                                                        250, 64, 113, 1),
                                                    minimumSize: Size(40.0, 45.0),
                                                    shape:
                                                    new RoundedRectangleBorder(
                                                        borderRadius:
                                                        new BorderRadius
                                                            .circular(
                                                            5.0))),
                                                onPressed: () {
                                                  Navigator.pushReplacement(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            MyHomePage()),
                                                  );
                                                },
                                              )
                                            ]);
                                      });
                                  showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        content: Text("Apakah Anda yakin?"),
                                        actions: [
                                          ElevatedButton(
                                            child: Text('OK',
                                                style:
                                                TextStyle(color: Colors.white)),
                                            style: ElevatedButton.styleFrom(
                                                primary:
                                                Color.fromRGBO(250, 64, 113, 1),
                                                minimumSize: Size(40.0, 45.0),
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                    new BorderRadius.circular(
                                                        5.0))),
                                            onPressed: () async {
                                              setState(() {
                                                dataBool = fetchPrefs();
                                              });
                                              Navigator.of(context).pop();
                                              return showDialog(
                                                context: context, // <<----
                                                barrierDismissible: false,
                                                builder: (BuildContext context) {
                                                  return AlertDialog(
                                                    content: Text(
                                                        "BERHASIL $buttonText"),
                                                    actions: [
                                                      ElevatedButton(
                                                        child: Text('OK',
                                                            style: TextStyle(
                                                                color:
                                                                Colors.white)),
                                                        style: ElevatedButton.styleFrom(
                                                            primary: Color.fromRGBO(
                                                                250, 64, 113, 1),
                                                            minimumSize:
                                                            Size(40.0, 45.0),
                                                            shape: RoundedRectangleBorder(
                                                                borderRadius:
                                                                new BorderRadius
                                                                    .circular(
                                                                    5.0))),
                                                        onPressed: () {
                                                          Navigator.pushReplacement(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder: (context) =>
                                                                      MyHomePage()));
                                                        },
                                                      )
                                                    ],
                                                  );
                                                },
                                              );
                                            },
                                          ),
                                          ElevatedButton(
                                            child: Text('BATAL',
                                                style:
                                                TextStyle(color: Colors.white)),
                                            style: ElevatedButton.styleFrom(
                                                primary:
                                                Color.fromRGBO(250, 64, 113, 1),
                                                minimumSize: Size(40.0, 45.0),
                                                shape: new RoundedRectangleBorder(
                                                    borderRadius:
                                                    new BorderRadius.circular(
                                                        5.0))),
                                            onPressed: () =>
                                                Navigator.of(context).pop(),
                                          )
                                        ],
                                      );
                                    },
                                  );
                                },
                              ),
                            ],
                          );
                        }),
                  ),
                );
              }

              return CircularProgressIndicator();
            }),
      ),
    );
  }

  Padding sliderMethod(TextStyle _styleQue) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
        child: Column(
          children: [
            Text(
              "Bagaimana kondisi fisik Anda hari ini?",
              style: _styleQue,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('\u{1F637}'),
                Slider(
                  value: _currentPhysicalValue,
                  min: 1,
                  max: 5,
                  divisions: 4,
                  label: _currentPhysicalLabel,
                  activeColor: Colors.pink.shade400,
                  onChanged: (double value) {
                    setState(() {
                      _currentPhysicalValue = value;
                      _currentPhysicalLabel = _physicalConditions[
                      _currentPhysicalValue.round().toInt()];
                    });
                  },
                ),
                Text('\u{1F603}'),
              ],
            ),
            Text(
              "Bagaimana mood Anda hari ini?",
              style: _styleQue,
            ),
            Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('\u{1F628}'),
                  Slider(
                    value: _currentSliderValue,
                    min: 1,
                    max: 5,
                    divisions: 4,
                    label: _currentLabel,
                    activeColor: Colors.pink.shade400,
                    onChanged: (double value) {
                      setState(() {
                        _currentSliderValue = value;
                        _currentLabel = _mentalConditions[
                        _currentSliderValue.round().toInt()];
                      });
                    },
                  ),
                  Text('\u{1F603}'),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<DataLokasi> getCurrentAddress() async {
    var lokasi;
    var gpsLocationObj = GetIt.instance.get<BaseGpsLocation>();

    currentPos = await gpsLocationObj.getCurrentPosition();
    try {
      lokasi = await gpsLocationObj.findAddressesFromCoordinates(currentPos.latitude, currentPos.longitude);
    } catch (e) {
      print(e);
    }
    return DataLokasi(lokasi: lokasi[0]);
  }

  OutlinedButton cameraButton(BuildContext context) {
    return OutlinedButton(
        onPressed: () async {
          await pressCamera(context);
        },
        child: Stack(
          children: <Widget>[
            Align(
                alignment: Alignment.centerRight,
                child: Icon(Icons.photo_camera,
                    color: Color.fromRGBO(250, 64, 113, 1))),
            Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Foto Selfie",
                  textAlign: TextAlign.left,
                ))
          ],
        ),
        style: OutlinedButton.styleFrom(
            primary: Colors.black,
            minimumSize: Size(400.0, 55.0),
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(4.0)),
            side: BorderSide(color: Color.fromRGBO(250, 64, 113, 1))));
  }

  Future<void> pressCamera(BuildContext context) async {
    WidgetsFlutterBinding.ensureInitialized();
    final cameras = await availableCameras();
    final firstCamera = cameras[1];
    imgPath = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => TakePictureScreen(
          camera: firstCamera,
        ),
      ),
    );
    imgPath = imgPath;
    // print(imgPath);
    setState(() {});
  }
}

class DataLokasi {
  Address lokasi;
  DataLokasi({required this.lokasi});
}

class DataFeedback {
  String? time;
  DataFeedback({required this.time});
}

class DataRequired {
  bool clockin;
  bool fillmental;

  DataRequired({
    required this.clockin,
    required this.fillmental,
  });
}
