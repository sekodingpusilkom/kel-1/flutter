import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simas/widget/general/appbar_widget.dart';
import 'package:simas/widget/presence/presence_bloc.dart';

import 'presence_contain.dart';

class PresencePage extends StatelessWidget {
  Widget build(context) {
    return Scaffold(
      appBar: headerNav(),
      body: BlocProvider<PresenceBloc>(
        create: (context) => PresenceBloc(),
        child: PresenceContain(),
      ),
    );
  }
}
