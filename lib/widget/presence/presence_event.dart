abstract class PresenceEvent {}

class PresenceWithButtonPressed extends PresenceEvent {
  final String? userId;
  final String imagePath;
  final double latitude;
  final double longitude;
  final List<int> mental;
  final int theUrl;

  PresenceWithButtonPressed(this.userId, this.imagePath, this.latitude,
      this.longitude, this.mental, this.theUrl);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PresenceWithButtonPressed &&
          runtimeType == other.runtimeType &&
          userId == other.userId &&
          imagePath == other.imagePath &&
          latitude == other.latitude &&
          longitude == other.longitude &&
          mental == other.mental;

  @override
  int get hashCode =>
      userId.hashCode ^
      imagePath.hashCode ^
      latitude.hashCode ^
      longitude.hashCode ^
      mental.hashCode;
}

class CheckMental extends PresenceEvent {
  // final String? userId;
  // final bool mental;
  // final bool login;

  // CheckMental(this.userId, this.mental, this.login);
  CheckMental();

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CheckMental && runtimeType == other.runtimeType;

  @override
  int get hashCode => ''.hashCode;
}

class CheckPhysical extends PresenceEvent {
  CheckPhysical();

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CheckPhysical && runtimeType == other.runtimeType;

  @override
  int get hashCode => ''.hashCode;
}
