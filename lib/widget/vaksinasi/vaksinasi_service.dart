// import 'dart:convert';
// import 'package:get_it/get_it.dart';
import 'dart:convert';

// import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';
import 'package:simas/models/commons/exception_constant.dart';
// import 'package:http_parser/http_parser.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:simas/models/user.dart';
// import 'package:simas/widget/authentication/authentication_exception.dart';
import 'package:simas/models/commons/url_constant.dart' as ConstantUrl;
import 'package:simas/widget/multipartreq_provider.dart';

abstract class VaksinasiService {
  Future<void> submitVaksinasi(String userId, String vaccineType, String number, String file, String date, String? token);
  getListVaccineType(String? token);
}

class VaksinasiServiceImpl extends VaksinasiService {
  // Client client = new Client();

  @override
  Future<void> submitVaksinasi(String userId, String vaccineType, String number, String file, String date, String? token) async {
    if(userId == "" || vaccineType == "" || number == "" || file == ""){
      throw Exception(ExceptionConstant.VAKSINASI_FAILED);
    }
    try {
      final request = new MultipartRequest("POST", Uri.parse(ConstantUrl.VACCINE));
      request.headers['authorization'] = "Bearer $token";
      request.headers['content-type'] = "multipart/form-data";
      request.fields['users_id'] = userId;
      request.fields['vaccineDate'] = date;
      request.fields['number'] = number;
      request.fields['vaccineType'] = vaccineType;
      request.files.add(await MultipartFile.fromPath(
        'certificate',file, contentType: new MediaType('image', 'jpg'),
      ));
      var reqObj = GetIt.instance.get<BaseMultiPartReq>();
      var response = await reqObj.sendRequest(request);
      if (response.statusCode != 200) {
        throw Exception(ExceptionConstant.VAKSINASI_FAILED);
      }
    } catch (e) {
      throw Exception(ExceptionConstant.VAKSINASI_FAILED);
    }
  }

  getListVaccineType(String? token) async {
    try{
      var client = GetIt.instance.get<Client>();
      var response = await client.get(Uri.parse(ConstantUrl.VACCINE_TYPE),  headers: {
        "content-type": "application/json",
        "accept": "application/json",
        "authorization": "Bearer $token"
      });

      if(response.statusCode == 200){
        var data = json.decode(response.body);
        var vaccineList = [];
        for(var i=0;i<data.length;i++){
          // var singleData = data[i];
          // print(data[i]['name']);
          vaccineList.add({"display": data[i]['name'],"value": data[i]['id'].toString()});
        }
        return vaccineList;
      }

      else throw Exception(ExceptionConstant.VAKSINASI_TYPE_FAILED);
    }
    catch(e){
      throw Exception(ExceptionConstant.VAKSINASI_TYPE_FAILED);
    }
  }

}
