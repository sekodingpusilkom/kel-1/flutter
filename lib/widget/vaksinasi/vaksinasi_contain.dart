// @dart=2.9
// ignore_for_file: unused_field
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_file_picker/form_builder_file_picker.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simas/models/commons/exception_constant.dart';
import 'package:simas/models/commons/method_common.dart';
import 'package:simas/widget/riwayat_vaksinasi/riwayat_vaksinasi_page.dart';
import 'package:simas/widget/vaksinasi/bloc/vaksinasi_bloc.dart';

class VaksinasiContain extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
      child: Center(
          child: _VaccinationForm(),
      )),
      // ),
    );
  }
}

class _VaccinationForm extends StatefulWidget {
  @override
  __VaccinationFormState createState() => __VaccinationFormState();
}

class __VaccinationFormState extends State<_VaccinationForm> {

  final GlobalKey<FormBuilderState> _key = GlobalKey<FormBuilderState>();
  final _numberController = TextEditingController();
  final _dateController = TextEditingController();
  final format = DateFormat("yyyy-MM-dd");

  String dropdownValue = 'One';

  String vaccineType = '';

  VaksinasiBloc _bloc;
  void initState() {
    super.initState();
    _bloc = BlocProvider.of<VaksinasiBloc>(context)..add(VaksinasiDdlEvent());
  }
  var dataDdl = [];
  @override
  Widget build(BuildContext context) {
    final _vaksinBloc = BlocProvider.of<VaksinasiBloc>(context);

    _onSubmitButtonPressed() async {
      String number = _numberController.text.trim();
      String date = _dateController.text;

      if(number == '') {
        return showDialogBox(context, "Mohon isi vaksin ke berapa");
      }
      if(date == '') {
        return showDialogBox(context, "Mohon isi tanggal vaksinasi");
      }
      if(vaccineType == ''){
        return showDialogBox(context, "Mohon isi tipe vaksin yang dipakai");
      }
      var fileFields = _key.currentState.fields['file'];
      var filePath ="";
      if(fileFields.value !=  null){
        filePath = fileFields.value[0].path;
      }
      else{
        return showDialogBox(context, "Mohon isi sertifikat vaksin");
      }

      SharedPreferences prefs = await SharedPreferences.getInstance();
      _vaksinBloc.add(VaksinasiPostData(userId:  prefs.getString("userId"),number: number, date: date, file: filePath, vaccineType: vaccineType));
    }

    return BlocListener<VaksinasiBloc, VaksinasiState>(listener: (context, state) {
    if (state is VaksinasiStateFailure) {
     showDialogBox(context, state.error);
    }
    else if (state is VaksinasiStateSuccess) {
      showDialogBox(context, ExceptionConstant.VAKSINASI_SUC);
    }
    },
    child: BlocBuilder<VaksinasiBloc, VaksinasiState>(builder: (context, state) {
    if (state is VaksinasiStateLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    else if(state is VaksinasiStateGetDdlSuccess){
      dataDdl = state.dataDdl;
    }
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: FormBuilder(
        key: _key,
        child: Padding(
            padding: const EdgeInsets.all(12.0),
              child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Color.fromRGBO(250, 64, 113, 1)),
                    color: Colors.white
                  ),
                  child: DropDownFormField(
                    titleText: 'Jenis Vaksin',
                    hintText: 'Pilih jenis vaksin',
                    value: vaccineType,
                    onChanged: (value) {
                      setState(() {
                        vaccineType = value;
                      });
                    },
                    dataSource: dataDdl,
                    textField: 'display',
                    valueField: 'value',
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  key: Key("number"),
                  style: TextStyle(fontSize: 18,color: Colors.black),
                  decoration: InputDecoration(
                    labelText: 'Vaksinasi ke-',
                    labelStyle: TextStyle(
                      color: Colors.black
                    ),
                    hintStyle: TextStyle(
                      color: Colors.white
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromRGBO(250, 64, 113, 1)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromRGBO(250, 64, 113, 1)),
                    ),
                    suffixIcon: new Icon(
                      Icons.assignment_turned_in, color: Color.fromRGBO(250, 64, 113, 1)
                    ),
                  ),
                  controller: _numberController,
                  autocorrect: false,
                  validator: (value) {
                    if (value == null || value == "") {
                      return 'Data diperlukan.';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                DateTimeField(
                  format: format,
                  controller: _dateController,
                  style: TextStyle(fontSize: 18,color: Colors.black),
                  decoration: InputDecoration(
                    labelText: 'Date',
                    labelStyle: TextStyle(
                      color: Colors.black
                    ),
                    hintStyle: TextStyle(
                      color: Colors.white
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromRGBO(250, 64, 113, 1)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromRGBO(250, 64, 113, 1)),
                    ),
                    suffixIcon: new Icon(
                      Icons.access_time, color: Color.fromRGBO(250, 64, 113, 1)
                    ),
                  ),
                  onShowPicker: (context, currentValue) {
                    return showDatePicker(
                        context: context,
                        firstDate: DateTime(1900),
                        initialDate: currentValue ?? DateTime.now(),
                        lastDate: DateTime(2100));
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                FormBuilderFilePicker(
                  name: "file",
                  decoration: InputDecoration(
                    labelText: "Attachments",
                    labelStyle: TextStyle(
                      color: Colors.black
                    ),
                    hintStyle: TextStyle(
                      color: Colors.white
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromRGBO(250, 64, 113, 1)),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Color.fromRGBO(250, 64, 113, 1)),
                    ),
                    suffixIcon: new Icon(
                      Icons.file_upload, color: Color.fromRGBO(250, 64, 113, 1)
                    ),
                  ),
                  maxFiles: null,
                  previewImages: true,
                  onChanged: (val) => print(val),
                  selector: Row(
                    children: <Widget>[
                      Icon(Icons.file_upload),
                      Text('Upload'),
                    ],
                  ),
                  onFileLoading: (val) {
                    print(val);
                  },
                ),
                const SizedBox(
                  height: 16,
                ),
                ElevatedButton(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment:
                    MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            5, 0, 5, 0),
                      ),
                      Text(
                        'Riwayat Vaksinasi',
                      )
                    ],
                  ),

                  style: ElevatedButton.styleFrom(
                    primary:
                    Color.fromRGBO(250, 64, 113, 1),
                    minimumSize: Size(300.0, 45.0),
                  ),  onPressed: () async => {
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => RiwayatVaksinasiPage(),
                      )),
                    }
                ),
                Padding(
                  padding: EdgeInsets.only(top: 15.0),
                ),
                ElevatedButton(
                  key: Key("submitBtn"),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment:
                    MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(
                            5, 0, 5, 0),
                      ),
                      Text(
                        'SUBMIT',
                      )
                    ],
                  ),
                  style: ElevatedButton.styleFrom(
                    primary:
                    Color.fromRGBO(250, 64, 113, 1),
                    minimumSize: Size(300.0, 45.0),
                  ), onPressed: state is VaksinasiStateLoading
                    ? () {}
                    : () async {
                  await _onSubmitButtonPressed();
                }

                ),
              ],
            ),
          ),
        ),
      );
    })
    );
  }

}
