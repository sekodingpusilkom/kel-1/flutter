part of 'vaksinasi_bloc.dart';

abstract class VaksinasiEvent {

}

class VaksinasiDdlEvent extends VaksinasiEvent {

}

class VaksinasiPostData extends VaksinasiEvent {
  final String userId;
  final String vaccineType;
  final String number;
  final String file;
  final String date;

  VaksinasiPostData({required this.userId, required this.vaccineType, required this.number, required this.date, required this.file});
}
