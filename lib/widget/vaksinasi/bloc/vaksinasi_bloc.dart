import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:simas/models/commons/exception_constant.dart';
import 'package:simas/widget/sharedprefs_provider.dart';
import 'package:simas/widget/vaksinasi/vaksinasi_service.dart';

part 'vaksinasi_event.dart';
part 'vaksinasi_state.dart';

class VaksinasiBloc extends Bloc<VaksinasiEvent, VaksinasiState> {
  VaksinasiService _vaksinasiService = new VaksinasiServiceImpl();
  VaksinasiBloc() : super(VaksinasiInitial());

  @override
  Stream<VaksinasiState> mapEventToState(VaksinasiEvent event) async* {
    var localPrefs = GetIt.instance.get<BaseSharedPrefs>();
    String token = await localPrefs.getString('token');

    if(event is VaksinasiPostData){
      yield* _mapSubmit(event, token);
    }
    else if(event is VaksinasiDdlEvent){
      yield* _mapgetDdl(event, token);
    }
  }

  Stream<VaksinasiState> _mapSubmit(VaksinasiPostData event, String token) async* {
    yield VaksinasiStateLoading();
    try {
      await _vaksinasiService.submitVaksinasi(event.userId, event.vaccineType, event.number, event.file, event.date, token);
      yield VaksinasiStateSuccess();
    }
    catch (err) {
      yield VaksinasiStateFailure(ExceptionConstant.VAKSINASI_FAILED);
    }
  }
  Stream<VaksinasiState> _mapgetDdl(VaksinasiDdlEvent event, String token) async* {
    var data = await _vaksinasiService.getListVaccineType(token);
    yield VaksinasiStateGetDdlSuccess(data);
  }
}
