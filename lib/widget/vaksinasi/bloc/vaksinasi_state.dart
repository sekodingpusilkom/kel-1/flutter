part of 'vaksinasi_bloc.dart';

abstract class VaksinasiState {
  const VaksinasiState();

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is VaksinasiState && runtimeType == other.runtimeType;

  @override
  int get hashCode => 0;
}

class VaksinasiInitial extends VaksinasiState {
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      super == other &&
          other is VaksinasiInitial &&
          runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode;


}

class VaksinasiStateLoading extends VaksinasiState {
  VaksinasiStateLoading();

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      super == other &&
          other is VaksinasiStateLoading &&
          runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode;
}

class VaksinasiStateSuccess extends VaksinasiState {
  VaksinasiStateSuccess();

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      super == other &&
          other is VaksinasiStateSuccess &&
          runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode;
}

class VaksinasiStateFailure extends VaksinasiState {
  final String error;

  VaksinasiStateFailure(this.error);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      super == other &&
          other is VaksinasiStateFailure &&
          runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode ^ error.hashCode;
}

class VaksinasiStateGetDdlSuccess extends VaksinasiState{
  var dataDdl;

  VaksinasiStateGetDdlSuccess(this.dataDdl);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      super == other &&
          other is VaksinasiStateGetDdlSuccess &&
          runtimeType == other.runtimeType;

  @override
  int get hashCode => super.hashCode;
}
