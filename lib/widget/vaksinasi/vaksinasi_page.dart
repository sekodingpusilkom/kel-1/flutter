// ignore_for_file: import_of_legacy_library_into_null_safe
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simas/widget/general/appbar_widget.dart';
import 'package:simas/widget/vaksinasi/bloc/vaksinasi_bloc.dart';

import 'vaksinasi_contain.dart';

class VaksinasiPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: headerNav(),
      body:BlocProvider<VaksinasiBloc>(
        create: (context) => VaksinasiBloc(),
        child: VaksinasiContain(),
      ),
      // bottomNavigationBar: theBottomNav(),
    );
  }
}