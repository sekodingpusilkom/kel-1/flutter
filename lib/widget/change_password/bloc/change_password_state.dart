part of 'change_password_bloc.dart';

abstract class ChangePasswordState extends Equatable {
  const ChangePasswordState();

  @override
  List<Object> get props => [];
}

class ChangePasswordInitial extends ChangePasswordState {}

class ChangePasswordLoadingState extends ChangePasswordState {}

class ChangePasswordSuccessState extends ChangePasswordState {}

class ChangePasswordErrorState extends ChangePasswordState {}

class ChangePasswordEmptyState extends ChangePasswordState {}

class ChangePasswordFetchingState extends ChangePasswordState {}

class ChangePasswordFetchedState extends ChangePasswordState {}
