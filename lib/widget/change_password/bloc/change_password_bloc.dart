import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../change_password_service.dart';

part 'change_password_event.dart';
part 'change_password_state.dart';

class ChangePasswordBloc
    extends Bloc<ChangePasswordEvent, ChangePasswordState> {
  ChangePasswordBloc() : super(ChangePasswordInitial());
  ChangePasswordServiceImpl _service = ChangePasswordServiceImpl();
  @override
  Stream<ChangePasswordState> mapEventToState(
    ChangePasswordEvent event,
  ) async* {
    yield ChangePasswordLoadingState();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (event is SubmitButtonChangedEvent) {
      yield ChangePasswordLoadingState();
      String? message;
      await _service
          .submitChangePassword(
        event.oldPassword,
        event.newPassword,
        prefs.getString("userId"),
        prefs.getString("token"),
      )
          .then((value) {
        message = value;
      });
      print(message);
      if (message == "success") {
        yield ChangePasswordSuccessState();
      } else if (message == "error") {
        yield ChangePasswordErrorState();
      } else {
        yield ChangePasswordInitial();
      }
    }
  }
}
