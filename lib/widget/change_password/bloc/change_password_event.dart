part of 'change_password_bloc.dart';

abstract class ChangePasswordEvent extends Equatable {
  const ChangePasswordEvent();

  @override
  List<Object> get props => [];
}

class SubmitButtonChangedEvent extends ChangePasswordEvent {
  final String oldPassword;
  final String newPassword;

  SubmitButtonChangedEvent({
    required this.oldPassword,
    required this.newPassword,
  });
}

class PasswordLamaChanged extends ChangePasswordEvent {
  const PasswordLamaChanged();

  @override
  List<Object> get props => [];
}

class PasswordBaruFirstChanged extends ChangePasswordEvent {
  const PasswordBaruFirstChanged();

  @override
  List<Object> get props => [];
}

class PasswordBaruSecondChanged extends ChangePasswordEvent {
  const PasswordBaruSecondChanged();

  @override
  List<Object> get props => [];
}
