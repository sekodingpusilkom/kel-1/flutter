import 'dart:convert';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:simas/models/commons/url_constant.dart' as ConstantUrl;

abstract class ChangePasswordService {
  Future<String> submitChangePassword(
    String oldPassword,
    String newPassword,
    String? userId,
    String? token,
  );
}

class ChangePasswordServiceImpl extends ChangePasswordService {
  Client client = new Client();

  @override
  Future<String> submitChangePassword(
    String oldPassword,
    String newPassword,
    String? userId,
    String? token,
  ) async {
    Client client = GetIt.instance.get<Client>();
    final response = await client.post(
        Uri.parse(ConstantUrl.CHANGE_PASSWORD + "$userId"),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
        },
        body: json.encode(
            {'passwordLama': oldPassword, 'passwordBaru': newPassword}));
    print(response.statusCode);
    if (response.statusCode == 200) {
      return "success";
    } else {
      print('Else Submit');
      return "error";
    }
  }
}
