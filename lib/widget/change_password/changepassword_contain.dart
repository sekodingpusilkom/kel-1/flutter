import 'package:flutter/material.dart';

class ChangePasswordContain extends StatefulWidget {
  const ChangePasswordContain({
    Key? key,
  }) : super(key: key);

  @override
  _ChangePasswordContainState createState() => _ChangePasswordContainState();
}

class _ChangePasswordContainState extends State<ChangePasswordContain> {
  final _formKey = GlobalKey<FormState>();
  bool _isPasswordHidden = true;
  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Center(
            child: Container(
              width: 300,
              alignment: Alignment.center,
              padding: EdgeInsets.all(12),
              child: Column(
                children: [
                  Text(
                    'Ganti Password',
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 25),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Password Lama',
                        suffixIcon: IconButton(
                            icon: _isPasswordHidden
                                ? Icon(Icons.visibility_off)
                                : Icon(Icons.visibility_sharp),
                            onPressed: () {
                              setState(() {
                                _isPasswordHidden = !_isPasswordHidden;
                              });
                            })),
                    obscureText: _isPasswordHidden,
                    validator: (value) {
                      if (value == null || value == "" || value.length < 8) {
                        return 'Password minimal 8 karakter';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Password Baru',
                        suffixIcon: IconButton(
                            icon: _isPasswordHidden
                                ? Icon(Icons.visibility_off)
                                : Icon(Icons.visibility_sharp),
                            onPressed: () {
                              setState(() {
                                _isPasswordHidden = !_isPasswordHidden;
                              });
                            })),
                    obscureText: _isPasswordHidden,
                    validator: (value) {
                      if (value == null || value == "" || value.length < 8) {
                        return 'Password minimal 8 karakter';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        hintText: 'Ulangi Password Baru',
                        suffixIcon: IconButton(
                            icon: _isPasswordHidden
                                ? Icon(Icons.visibility_off)
                                : Icon(Icons.visibility_sharp),
                            onPressed: () {
                              setState(() {
                                _isPasswordHidden = !_isPasswordHidden;
                              });
                            })),
                    obscureText: _isPasswordHidden,
                    validator: (value) {
                      if (value == null || value == "" || value.length < 8) {
                        return 'Password minimal 8 karakter';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  ElevatedButton(
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        ),
                        Text(
                          'SUBMIT',
                        )
                      ],
                    ),
                    style: ElevatedButton.styleFrom(
                      primary: Color.fromRGBO(250, 64, 113, 1),
                      minimumSize: Size(300.0, 50.0),
                    ),
                    onPressed: () {},
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
