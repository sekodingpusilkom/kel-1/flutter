import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simas/widget/general/appbar_widget.dart';
// import 'package:simas/widget/general/appbar_widget.dart';

import 'bloc/change_password_bloc.dart';
import 'change_password_contain.dart';

class ChangePasswordPage extends StatelessWidget {
  Widget build(context) {
    return Scaffold(
      appBar: headerNav(),
      body: BlocProvider(
        create: (context) => ChangePasswordBloc(),
        child: ChangePasswordContain(),
      ),
      // bottomNavigationBar: theBottomNav(),
    );
  }
}
