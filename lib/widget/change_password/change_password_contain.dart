import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/change_password_bloc.dart';

class ChangePasswordContain extends StatefulWidget {
  @override
  _ChangePasswordContainState createState() => _ChangePasswordContainState();
}

class _ChangePasswordContainState extends State<ChangePasswordContain> {
  TextEditingController _newPassword = TextEditingController();
  TextEditingController _confirmPassword = TextEditingController();
  TextEditingController _oldPassword = TextEditingController();
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  bool _isObscureLama = true;
  bool _isObscureBaru = true;
  bool _isObscureValidate = true;
  @override
  Widget build(BuildContext context) {
    final _changePasswordBloc = BlocProvider.of<ChangePasswordBloc>(context);
    return BlocListener<ChangePasswordBloc, ChangePasswordState>(
      listener: (context, state) {
        var textStyle = TextStyle(color: Color.fromRGBO(250, 64, 113, 1));
        if (state is ChangePasswordLoadingState) {
          AlertDialog alert = AlertDialog(
            title: Text("Proses"),
            content: LinearProgressIndicator(),
          );
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return alert;
            },
          );
        } else if (state is ChangePasswordSuccessState) {
          Navigator.of(context).pop();
          AlertDialog alert = AlertDialog(
            title: Text("Sukses"),
            content: Text("Password Berhasil Diganti"),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  },
                  child: Text('OK', style: textStyle))
            ],
          );

          showDialog(
            context: context,
            builder: (BuildContext context) {
              return alert;
            },
          );
        } else if (state is ChangePasswordErrorState) {
          Navigator.of(context).pop();
          AlertDialog alert = AlertDialog(
            title: Text("Error"),
            content: Text("Password Gagal Diganti"),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('OK', style: textStyle))
            ],
          );

          showDialog(
            context: context,
            builder: (BuildContext context) {
              return alert;
            },
          );
        }
      },
      child: BlocBuilder<ChangePasswordBloc, ChangePasswordState>(
        builder: (context, state) {
          return SafeArea(
            child: SingleChildScrollView(
              child: Form(
                key: _formkey,
                child: Container(
                  padding: EdgeInsets.all(50.0),
                  child: Column(
                    children: [
                      Text(
                        'Ganti Password',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20.0),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 30.0),
                      ),
                      TextFormField(
                        controller: _oldPassword,
                        obscureText: _isObscureLama,
                        decoration: InputDecoration(
                          labelText: 'Password Lama',
                          border: OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(4.0)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color.fromRGBO(250, 64, 113, 1))),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color.fromRGBO(250, 64, 113, 1))),
                          suffixIcon: IconButton(
                              icon: Icon(_isObscureLama
                                  ? Icons.visibility_off
                                  : Icons.visibility),
                              onPressed: () {
                                setState(() {
                                  _isObscureLama = !_isObscureLama;
                                });
                              }),
                        ),
                        onChanged: (value) {},
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 30.0),
                      ),
                      TextFormField(
                        controller: _newPassword,
                        obscureText: _isObscureBaru,
                        decoration: InputDecoration(
                            labelText: 'Password Baru',
                            border: OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(4.0)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color.fromRGBO(250, 64, 113, 1))),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color.fromRGBO(250, 64, 113, 1))),
                            suffixIcon: IconButton(
                                icon: Icon(_isObscureBaru
                                    ? Icons.visibility_off
                                    : Icons.visibility),
                                onPressed: () {
                                  setState(() {
                                    _isObscureBaru = !_isObscureBaru;
                                  });
                                })),
                        validator: (value) {
                          if (value == null ||
                              value == "" ||
                              value.length < 8) {
                            return 'Password minimal 8 karakter';
                          }
                          return null;
                        },
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 30.0),
                      ),
                      TextFormField(
                        controller: _confirmPassword,
                        obscureText: _isObscureValidate,
                        decoration: InputDecoration(
                            labelText: 'Konfirmasi Password',
                            border: OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(4.0)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color.fromRGBO(250, 64, 113, 1))),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Color.fromRGBO(250, 64, 113, 1))),
                            suffixIcon: IconButton(
                                icon: Icon(_isObscureValidate
                                    ? Icons.visibility_off
                                    : Icons.visibility),
                                onPressed: () {
                                  setState(() {
                                    _isObscureValidate = !_isObscureValidate;
                                  });
                                })),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Masukan kembali password baru Anda';
                          }
                          if (_newPassword.text != _confirmPassword.text) {
                            return "Password tidak cocok";
                          }
                          return null;
                        },
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 20.0),
                      ),
                      ElevatedButton(
                        child: Text(
                          'SUBMIT',
                          style: TextStyle(color: Colors.white),
                        ),
                        style: ElevatedButton.styleFrom(
                            primary: Color.fromRGBO(250, 64, 113, 1),
                            minimumSize: Size(300.0, 45.0),
                            shape: new RoundedRectangleBorder(
                                borderRadius: new BorderRadius.circular(5.0))),
                        onPressed: () {
                          var passLama = _oldPassword.text;
                          var passBaru = _newPassword.text;
                          _changePasswordBloc.add(
                            SubmitButtonChangedEvent(
                                oldPassword: passLama, newPassword: passBaru),
                          );
                          setState(() {});
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
