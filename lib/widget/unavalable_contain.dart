import 'package:flutter/material.dart';

class UnavailableContain extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: [
          Image(
            image: AssetImage('assets/images/unavailable-page.png'),
            width: MediaQuery.of(context).size.width * 0.8,
          ),
        ],
      )),
    );
  }
}
