import 'dart:async';

import 'package:bloc/bloc.dart';
import 'history_event.dart';
import 'history_state.dart';
import 'package:simas/models/history.dart';
import 'package:simas/repository/repository_history.dart';

class HistoryBloc extends Bloc<HistoryEvent, HistoryState> {
  final HistoryRepository historyRepository;
  HistoryBloc({required this.historyRepository})
      : super(HistoryUninitializedState());

  @override
  Stream<HistoryState> mapEventToState(HistoryEvent event) async* {
    List<HistoryClockIn> history = List.empty(growable: true);
    yield HistoryLoadingState();
    try {
      if (event is SubmitButtonChangedEvent) {
        history = await historyRepository.fetchHistoryClockIn(
            event.userId, event.startDate, event.endDate);
      } else if (event is SelectDateEvent) {
        yield HistoryGetDateState();
        return;
      } else if (event is DetailButtonEvent) {
        print("Hitting service");
      }
      if (history.length == 0) {
        yield HistoryEmptyState();
      } else {
        yield HistoryFetchedState(history: history);
      }
    } catch (_) {
      yield HistoryErrorState();
    }
  }
}
