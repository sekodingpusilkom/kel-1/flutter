import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simas/repository/repository_history.dart';
// import 'package:simas/widget/general/appbar_widget.dart';
import 'history_bloc.dart';

import 'history_contain.dart';

class HistoryPage extends StatelessWidget {
  Widget build(context) {
    return Scaffold(
      // appBar: headerNav(),
      body: BlocProvider<HistoryBloc>(
        create: (context) =>
            HistoryBloc(historyRepository: new HistoryRepository()),
        child: HistoryContain(),
      ),
    );
  }
}
