// ignore_for_file: override_on_non_overriding_member, duplicate_ignore

import 'dart:convert';
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';
import 'package:simas/models/commons/url_constant.dart' as ConstantUrl;

abstract class HistoryService {
  Future<String> getHistory(String? userId, String imagePath, double latitude,
      double longitude, String? token, List<int> mental, int theUrl);
}

class FakeHistoryService extends HistoryService {
  Client client = new Client();

  @override
  Future<String> getHistory(String? userId, String imagePath, double latitude,
      double longitude, String? token, List<int> mental, int theUrl) async {
    String savedTime = "";
    List<String> moment = ['start', 'end'];
    final request =
        new MultipartRequest("POST", Uri.parse(ConstantUrl.PRESENCE[theUrl]));
    print(ConstantUrl.PRESENCE[theUrl]);
    // print('lat_${moment[theUrl]}');

    request.headers['authorization'] = "Bearer $token";
    request.headers['content-type'] = "multipart/form-data";
    request.fields['users_id'] = userId!;
    request.fields['lat_${moment[theUrl]}'] = latitude.toString();
    request.fields['long_${moment[theUrl]}'] = longitude.toString();
    request.fields['mental'] = mental[0].toString();
    request.fields['physical'] = mental[1].toString();
    request.files.add(await MultipartFile.fromPath(
      'photo_${moment[theUrl]}',
      imagePath,
      contentType: new MediaType('image', 'jpg'),
    ));

    var response = await request.send();
    print('Kiriman FIle : ${request.fields}');
    print('Response code : ${response.statusCode}');
    var resDecode = await Response.fromStream(response);
    var test = json.decode(resDecode.body);
    if (response.statusCode == 200) {
      DateTime savedTime = DateTime.parse(test['time']);
      return '${savedTime.hour}:${savedTime.minute}:${savedTime.second}';
    } else if (response.statusCode == 400) {
      DateTime savedTime = DateTime.parse(test['error']);
      return '${savedTime.hour}:${savedTime.minute}:${savedTime.second}';
    } else {
      print('Else Submit');
      return savedTime;
    }
  }

  @override
  // ignore: override_on_non_overriding_member
  Future<bool> isCheckIn(String? userId, String? token) {
    // ignore: todo
    // TODO: implement isCheckIn
    throw UnimplementedError();
  }

  @override
  // ignore: override_on_non_overriding_member
  Future<bool> isFillIn(String? userId, String? token) {
    // ignore: todo
    // TODO: implement isFillIn
    throw UnimplementedError();
  }

  @override
  Future<String> submitPresence(
      String? userId,
      String imagePath,
      double latitude,
      double longitude,
      String? token,
      List<int> mental,
      int theUrl) {
    throw UnimplementedError();
  }
}
