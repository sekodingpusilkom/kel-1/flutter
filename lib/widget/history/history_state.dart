import 'package:simas/models/history.dart';

abstract class HistoryState {}

class HistoryUninitializedState extends HistoryState {}

class HistoryInitializedState extends HistoryState {}

class HistoryLoadingState extends HistoryState {}

class HistoryGetDateState extends HistoryState {}

class HistoryFetchedState extends HistoryState {
  final List<HistoryClockIn> history;
  HistoryFetchedState({required this.history});
}

class HistoryErrorState extends HistoryState {}

class HistoryEmptyState extends HistoryState {}

class HistoryDetailState extends HistoryState {
  final HistoryClockIn history;
  HistoryDetailState({required this.history});
}
