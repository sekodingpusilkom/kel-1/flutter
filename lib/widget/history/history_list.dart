import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:simas/models/history.dart';

import 'history_bloc.dart';
import 'history_detail.dart';
import 'history_state.dart';

class HistoryListing extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: BlocProvider.of<HistoryBloc>(context),
      builder: (context, state) {
        if (state is HistoryFetchedState) {
          final stateAsHistoryFetchedState = state;
          final histories = stateAsHistoryFetchedState.history;
          return buildHistoryList(histories, (HistoryClockIn history) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DetailPage(history: history)));
          });
        } else if (state is HistoryLoadingState) {
          return Expanded(child: Center(child: CircularProgressIndicator()));
        } else if (state is HistoryEmptyState) {
          return Expanded(child: Center(child: Text("Data belum ada")));
        } else {
          return Expanded(child: Center(child: Text("")));
        }
      },
    );
  }

  Widget buildHistoryList(List<HistoryClockIn> histories,
      void Function(HistoryClockIn history)? onPressedDetail) {
    return Expanded(
      child: ListView.separated(
        itemBuilder: (BuildContext context, index) {
          HistoryClockIn history = histories[index];
          return Container(
              child: cardView(history.users.name, history.startTime,
                  history.endTime, () => onPressedDetail!(history)));
        },
        separatorBuilder: (BuildContext context, index) {
          return Divider(
            height: 8.0,
            color: Colors.grey.shade100,
          );
        },
        itemCount: histories.length,
      ),
    );
  }

  Widget cardView(String title, DateTime mulai, DateTime akhir,
      void Function()? onPressedDetail) {
    return Center(
      child: Card(
        color: Colors.white,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              title: Text(title),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                      "Tanggal : ${new DateFormat("dd-MM-yyyy").format(mulai)}"),
                  Text(
                      "Waktu : ${new DateFormat("hh:mm aaa").format(mulai)} - ${new DateFormat("hh:mm aaa").format(akhir)}"),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                const SizedBox(width: 8),
                TextButton(
                  child: const Text(
                    'Detail',
                    style: TextStyle(color: Color.fromRGBO(250, 64, 113, 1)),
                  ),
                  onPressed: onPressedDetail,
                ),
                const SizedBox(width: 8),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
