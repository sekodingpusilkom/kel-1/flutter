import 'package:simas/models/history.dart';

abstract class HistoryEvent {}

class SubmitButtonChangedEvent extends HistoryEvent {
  final String? userId;
  final DateTime startDate;
  final DateTime endDate;

  SubmitButtonChangedEvent(
      {this.userId, required this.startDate, required this.endDate});
}

class DetailButtonEvent extends HistoryEvent {
  final HistoryClockIn history;

  DetailButtonEvent({required this.history});
}

class LoadingEvent extends HistoryEvent {}

class SelectDateEvent extends HistoryEvent {}
