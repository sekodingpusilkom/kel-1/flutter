import 'dart:convert';
import 'dart:ui';
import 'package:flutter/material.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:geocoder/geocoder.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simas/models/history.dart';
import 'package:simas/models/commons/url_constant.dart' as ConstantUrl;

import 'history_contain.dart';

class DetailPage extends StatefulWidget {
  final HistoryClockIn history;
  DetailPage({required this.history});

  @override
  State<StatefulWidget> createState() {
    return DetailPageState(history: this.history);
  }
}

class DetailPageState extends State<DetailPage> {
  final HistoryClockIn history;

  DetailPageState({required this.history});

  List<String> _mentalConditions = [
    '',
    '\u{1F628}',
    '\u{1F626}',
    '\u{1F60A}',
    '\u{1F606}',
    '\u{1F603}'
  ];

  List<String> _physicalConditions = [
    '',
    '\u{1F637}',
    '\u{1F635}',
    '\u{1F609}',
    '\u{1F60A}',
    '\u{1F604}',
  ];

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Center(
      child: Card(
        color: Colors.white,
        elevation: 50,
        child: SizedBox(
          width: 500,
          height: 800,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircleAvatar(
                        backgroundImage: NetworkImage(
                            "http://35.212.240.171/file/resources/clockin/${history.photoStart}"), //NetworkImage
                        radius: 70,
                      ),
                      Padding(padding: EdgeInsets.all(20)),
                      CircleAvatar(
                        backgroundImage: NetworkImage(
                            "http://35.212.240.171/file/resources/clockout/${history.photoEnd}"), //NetworkImage
                        radius: 70,
                      ),
                    ],
                  ),
                ),
                //CirclAvatar
                SizedBox(
                  height: 10,
                ),
                const Divider(
                  color: Color.fromRGBO(250, 64, 113, 1),
                ),
                ListTile(
                    title: Text(
                      'Tanggal',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    subtitle: Text(new DateFormat("dd-MM-yyyy")
                        .format(history.startTime))),
                // ListTile(
                //   title: Text(
                //     'Lokasi',
                //     style: TextStyle(fontWeight: FontWeight.bold),
                //   ),
                //   subtitle: FutureBuilder<DataLokasi>(
                //       future: getLocation(history.latStart, history.longStart),
                //       builder: (context, snapshot) {
                //         if (snapshot.hasData) {
                //           return Text("${snapshot.data!.lokasi.addressLine}");
                //         }
                //         return LinearProgressIndicator();
                //       }),
                // ),
                ListTile(
                  title: Text(
                    'Clock In',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Pukul : " +
                          new DateFormat("hh:mm aaa")
                              .format(history.startTime)),
                      FutureBuilder<DataLokasi>(
                          future:
                              getLocation(history.latStart, history.longStart),
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              return Text("Lokasi : " +
                                  "${snapshot.data!.lokasi.addressLine}");
                            }
                            return LinearProgressIndicator();
                          }),
                    ],
                  ),
                ),
                ListTile(
                  title: Text(
                    'Clock Out',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Pukul : " +
                          new DateFormat("hh:mm aaa").format(history.endTime)),
                      FutureBuilder<DataLokasi>(
                          future: getLocation(history.latEnd, history.longEnd),
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              return Text("Lokasi : " +
                                  "${snapshot.data!.lokasi.addressLine}");
                            }
                            return LinearProgressIndicator();
                          }),
                    ],
                  ),
                ),

                ListTile(
                  title: Text(
                    'Kondisi',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle: FutureBuilder<DataFisik>(
                    future: requestFisik(
                        history.users.id.toString(),
                        DateFormat("yyyy-MM-dd")
                            .format(history.startTime)
                            .toString()),
                    builder: (ctx, snap) {
                      if (snap.hasData) {
                        String mental = snap.data!.mental;
                        String fisik = snap.data!.fisik;
                        return Text(
                            'Fisik : ${_mentalConditions[int.parse(mental)]}  Mental : ${_physicalConditions[int.parse(fisik)]}');
                      }
                      return LinearProgressIndicator();
                    },
                  ),
                ), //SizedBox
                SizedBox(
                  height: 10,
                ), //SizedBoxBox
                SizedBox(
                  width: 100,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: Color.fromRGBO(250, 64, 113, 1),
                        minimumSize: Size(80.0, 45.0),
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(5.0))),
                    onPressed: () {
                      Navigator.pop(
                          context,
                          MaterialPageRoute(
                              builder: (context) => HistoryContain()));
                    },
                    child: Text('Kembali'),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    ));
  }

  Future<DataLokasi> getLocation(latitude, longitude) async {
    late List<Address> lokasi;
    try {
      lokasi = await Geocoder.local
          .findAddressesFromCoordinates(Coordinates(latitude, longitude));
    } catch (e) {
      print(e);
      lokasi[0] = Address(countryName: "-");
    }

    return DataLokasi(lokasi: lokasi[0]);
  }
}

class DataLokasi {
  Address lokasi;
  DataLokasi({required this.lokasi});
}

class DataFisik {
  String fisik;
  String mental;

  DataFisik({
    required this.fisik,
    required this.mental,
  });
}

Future<DataFisik> requestFisik(String? userId, String? tanggal) async {
  var prefs = await SharedPreferences.getInstance();
  String? token = prefs.getString("token");
  final request = new MultipartRequest(
      "GET", Uri.parse(ConstantUrl.FILL_DETAIL + "/$userId/$tanggal"));
  request.headers['authorization'] = "Bearer $token";

  var response = await request.send();
  // print("isi response status code : ${response.statusCode}");
  var resDecode = await Response.fromStream(response);
  // print(resDecode.body);
  if (response.statusCode == 200) {
    var requestDetailUser = json.decode(resDecode.body);
    // print("Is check in requestDetailUser : $requestDetailUser");
    return DataFisik(
        fisik: requestDetailUser['physical'],
        mental: requestDetailUser['mental']);
  } else {
    // print('Else requestDetailUser');
    return DataFisik(fisik: "0", mental: "0");
  }
}
