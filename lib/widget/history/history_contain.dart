import 'package:date_range_form_field/date_range_form_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'history_bloc.dart';
import 'history_event.dart';
import 'history_list.dart';

class HistoryContain extends StatefulWidget {
  @override
  _HistoryContainState createState() => _HistoryContainState();
}

GlobalKey<FormState> myFormKey = new GlobalKey();

class _HistoryContainState extends State<HistoryContain> {
  @override
  Widget build(BuildContext context) {
    final _historyBloc = BlocProvider.of<HistoryBloc>(context);
    return Scaffold(
      body: SafeArea(
        child: Form(
          key: myFormKey,
          child: Column(
            children: [
              SafeArea(
                child: DateRangeField(
                  initialEntryMode: DatePickerEntryMode.inputOnly,
                  firstDate: DateTime(2021),
                  enabled: true,
                  fieldStartHintText: 'Tanggal Mulai',
                  fieldStartLabelText: 'Tanggal Mulai',
                  fieldEndHintText: 'Tanggal Akhir',
                  fieldEndLabelText: 'Tanggal Akhir',
                  decoration: InputDecoration(
                    labelText: 'Rentang Tanggal',
                    prefixIcon: Icon(Icons.date_range),
                    hintText: 'Pilih tanggal mulai dan tanggal akhir',
                    enabledBorder: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0),
                      borderSide: BorderSide(
                        color: Color.fromRGBO(250, 64, 113, 1),
                      ),
                    ),
                    focusedBorder: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0),
                      borderSide: BorderSide(
                        color: Color.fromRGBO(250, 64, 113, 1),
                        style: BorderStyle.solid,
                      ),
                    ),
                  ),
                  validator: (value) {
                    if (value!.start.isBefore(DateTime.now())) {
                      return 'Please enter a later start date';
                    }
                    return null;
                  },
                  onSaved: (value) async {
                    var prefs = await SharedPreferences.getInstance();
                    String? userId = prefs.getString("userId");

                    _historyBloc.add(
                      SubmitButtonChangedEvent(
                          userId: userId,
                          startDate: value!.start,
                          endDate: value.end),
                    );
                  },
                ),
              ),
              ElevatedButton(
                child: Text(
                  'Submit',
                  style: TextStyle(color: Colors.white),
                ),
                style: ElevatedButton.styleFrom(
                    primary: Color.fromRGBO(250, 64, 113, 1),
                    minimumSize: Size(80.0, 45.0),
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(5.0))),
                onPressed: () {
                  _historyBloc.add(LoadingEvent());
                  final FormState? form = myFormKey.currentState;
                  form!.save();
                },
              ),
              Padding(
                padding: EdgeInsets.only(top: 30.0),
              ),
              HistoryListing(),
            ],
          ),
        ),
      ),
    );
  }
}
