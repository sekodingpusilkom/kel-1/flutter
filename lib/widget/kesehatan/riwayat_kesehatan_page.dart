import 'package:flutter/material.dart';
import 'package:simas/widget/general/appbar_widget.dart';

import 'riwayat_kesehatan_contain.dart';

class KesehatanPage extends StatelessWidget {
  Widget build(context) {
    return Scaffold(
      appBar: headerNav(),
      body: riwayatKesehatanContain(),
      // bottomNavigationBar: theBottomNav(),
    );
  }
}
