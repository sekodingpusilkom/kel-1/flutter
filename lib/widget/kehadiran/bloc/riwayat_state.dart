part of 'riwayat_bloc.dart';

abstract class RiwayatState {
  const RiwayatState();

}

class RiwayatInitial extends RiwayatState {}

class RiwayatValue extends RiwayatState {
  final Color color;

  RiwayatValue(this.color);

}
