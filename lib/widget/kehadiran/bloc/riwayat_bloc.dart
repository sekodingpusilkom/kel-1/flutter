import 'dart:async';
// import 'dart:html';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';

part 'riwayat_event.dart';
part 'riwayat_state.dart';

class RiwayatBloc extends Bloc<RiwayatEvent, RiwayatState> {
  RiwayatBloc() : super(RiwayatInitial());
  // Color _color =

  @override
  Stream<RiwayatState> mapEventToState(
    RiwayatEvent event,
  ) async* {
    if (event is ToBlue) {
      if (state is RiwayatInitial) {
        yield RiwayatValue(
          Color.fromRGBO(255, 0, 0, 0),
        );
      } else {
        yield RiwayatValue(
          Color.fromRGBO(0, 0, 255, 0),
        );
      }
    }
  }
}
