import 'package:flutter/material.dart';
import 'package:simas/widget/general/appbar_widget.dart';

import 'riwayat_kehadiran_contain.dart';

class ProfilePage extends StatelessWidget {
  Widget build(context) {
    return Scaffold(
      appBar: headerNav(),
      body: riwayatKehadiranContain(),
      // bottomNavigationBar: theBottomNav(),
    );
  }
}
