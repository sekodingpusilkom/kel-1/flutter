import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simas/widget/general/appbar_widget.dart';
import 'package:simas/widget/kehadiran/bloc/riwayat_bloc.dart';

Widget riwayatKehadiranContain() {
  return SafeArea(
    child: Scaffold(
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(backgroundColor: Colors.amber, onPressed: () {}),
          SizedBox(width: 10),
          FloatingActionButton(backgroundColor: Colors.blue, onPressed: () {}),
        ],
      ),
      appBar: headerNav(),
      body: BlocBuilder<RiwayatBloc, RiwayatState>(
        builder: (context, state) {
          return Center(
            child: AnimatedContainer(
              width: 100,
              height: 100,
              duration: Duration(microseconds: 500),
              // color: (state is RiwayatInitial)? state. : ,
            ),
          );
        },
      ),
    ),
  );
}
