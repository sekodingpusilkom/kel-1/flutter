import 'package:flutter/material.dart';

import 'general/appbar_widget.dart';
import 'unavalable_contain.dart';

class UnavailablePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: headerNav(),
      body: UnavailableContain(),
      // bottomNavigationBar: theBottomNav(),
    );
  }
}
