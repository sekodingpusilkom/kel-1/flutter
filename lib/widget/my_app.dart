// ignore_for_file: deprecated_member_use

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simas/widget/authentication/authentication_bloc.dart';
import 'package:simas/widget/authentication/authentication_state.dart';
import 'package:simas/widget/home/home_page.dart';
import 'package:simas/widget/login/login_page.dart';

class MyApp extends StatelessWidget {
  final _primer = Colors.pink.shade400;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
        // debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          // customize app theme here
          primaryColor: _primer,
          appBarTheme: AppBarTheme(
            color: Colors.pink.shade200,
            shadowColor: Colors.black87,
            textTheme: TextTheme(
                headline1: TextStyle(
              color: Colors.black12,
            )),
          ),
          bottomNavigationBarTheme: BottomNavigationBarThemeData(
            selectedItemColor: Colors.pink,
            unselectedItemColor: Colors.pink.shade300,
          ),
          buttonColor: _primer,
          floatingActionButtonTheme: FloatingActionButtonThemeData(
            focusColor: _primer,
            backgroundColor: _primer,
          ),
          cardTheme: CardTheme(
            color: _primer,
          ),
        ),
        home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
            builder: (context, state) {
          if (state is AuthenticationAuthenticated) {
            // show home page
            return MyHomePage();
          }
          return LoginChecker();
        }));
  }
}
