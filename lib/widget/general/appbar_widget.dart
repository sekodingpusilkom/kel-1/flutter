import 'package:flutter/material.dart';

AppBar headerNav() {
  return AppBar(
    title: Container(
      child: Image(
        image: AssetImage('assets/images/Sinarmas.png'),
        width: 120,
      ),
    ),
    elevation: 5,
    automaticallyImplyLeading: false,
  );
}
