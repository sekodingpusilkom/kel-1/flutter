abstract class AktivitasEvent {
}

class AktivitasSharePressed extends AktivitasEvent {
  final String imgUrl;
  final String desc;
  AktivitasSharePressed({required this.imgUrl, required this.desc});
}

class AktivitasEditPressed extends AktivitasEvent {
  final String postId;
  final String desc;
  final String imgUrl;
  final String userId;
  AktivitasEditPressed({required this.postId, required this.desc, required this.imgUrl, required this.userId});
}

class AktivitasDeletePressed extends AktivitasEvent {
  final String postId;
  AktivitasDeletePressed({required this.postId});
}

class AktivitasGetData extends AktivitasEvent{

}