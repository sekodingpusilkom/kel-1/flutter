import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simas/models/aktivitas_model.dart';
import 'package:simas/widget/aktivitas/aktivitas_bloc.dart';
import 'package:simas/widget/aktivitas/aktivitas_event.dart';
import 'package:simas/models/commons/url_constant.dart' as ConstantUrl;

FutureBuilder<void> aktivitasContain(
    BuildContext context, List<AktivitasModel> aktivitas) {
  final _aktBloc = BlocProvider.of<AktivitasBloc>(context);
  late SharedPreferences prefs;
  var _editDescFieldController = TextEditingController();

  _onShareData(BuildContext context, String desc, String imgUrl) async {
    _aktBloc.add(AktivitasSharePressed(imgUrl: imgUrl, desc: desc));
  }

  Future<void> _displayTextInputDialog(BuildContext context, String postId,
      String imgUrl, String desc, String userId) async {
    _editDescFieldController.text = desc;
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Edit deskripsi'),
            content: TextField(
              controller: _editDescFieldController,
              decoration: InputDecoration(hintText: "Text Field in Dialog"),
            ),
            actions: <Widget>[
              TextButton(
                  child: Text('Batal',
                      style: TextStyle(color: Color.fromRGBO(250, 64, 113, 1))),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
              TextButton(
                child: Text('Ok',
                    style: TextStyle(color: Color.fromRGBO(250, 64, 113, 1))),
                onPressed: () {
                  _aktBloc.add(AktivitasEditPressed(
                      postId: postId,
                      desc: _editDescFieldController.text,
                      imgUrl: imgUrl,
                      userId: userId));
                  Navigator.pop(context);
                },
              ),
            ],
          );
        });
  }

  Future<void> chooseEditOrDelete(BuildContext context, String postId,
      String imgUrl, String desc, String userId) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text("Aksi"),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    GestureDetector(
                      child: Text("Edit"),
                      onTap: () {
                        Navigator.of(context).pop();
                        _displayTextInputDialog(
                            context, postId, imgUrl, desc, userId);
                      },
                    ),
                    Padding(padding: EdgeInsets.all(8.0)),
                    GestureDetector(
                      child: Text("Delete"),
                      onTap: () {
                        _aktBloc.add(AktivitasDeletePressed(postId: postId));
                        Navigator.of(context).pop();
                      },
                    )
                  ],
                ),
              ));
        });
  }

  generateDetailBtn(String postId, String imgUrl, String desc, String userId) {
    return Container(
      alignment: Alignment.topRight,
      child: IconButton(
        tooltip: "lebih lanjut",
        onPressed: () async =>
            await chooseEditOrDelete(context, postId, imgUrl, desc, userId),
        icon: Icon(Icons.more_horiz, color: Theme.of(context).primaryColor),
      ),
    );
  }

  generateShareBtn(String userId, String desc, String imgUrl) {
    return Container(
      alignment: Alignment.topRight,
      child: IconButton(
        tooltip: "bagikan",
        onPressed: () => _onShareData(context, desc, imgUrl),
        icon: Icon(Icons.share, color: Theme.of(context).primaryColor),
      ),
    );
  }

  Future<bool> _getPrefs() async {
    prefs = await SharedPreferences.getInstance();
    return true;
  }

  generateSingle(BuildContext context, String? loggedUserId, String userId,
      String profileName, String imgUrl, String desc, String postId) {
    return Card(
        color: Colors.white,
        clipBehavior: Clip.antiAlias,
        child: Padding(
          padding: EdgeInsets.all(6.0),
          child: Column(
            children: [
              if (loggedUserId != null && loggedUserId == userId)
                generateDetailBtn(postId, imgUrl, desc, userId),
              ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: Image.network(imgUrl, errorBuilder:
                      (BuildContext context, Object exception,
                          StackTrace? stackTrace) {
                    return SizedBox(
                        height: 200.0,
                        width: 100.0,
                        child: Icon(Icons.error, size: 60.0));
                  })),
              if (loggedUserId != null && loggedUserId == userId)
                generateShareBtn(userId, desc, imgUrl),
              Container(
                alignment: Alignment.topLeft,
                child: Text(
                  profileName,
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                alignment: Alignment.topLeft,
                child: Text(
                  desc,
                  style: TextStyle(fontSize: 16.0),
                ),
              ),
            ],
          ),
        ));
  }

  List<Widget> posts = [];

  return FutureBuilder(
    future: _getPrefs(),
    builder: (context, snapshot) {
      if (snapshot.hasData) {
        posts = [];
        aktivitas.forEach((e) => {
              posts.add(generateSingle(
                  context,
                  prefs.getString("userId"),
                  e.usersId.toString(),
                  e.name,
                  ConstantUrl.IMG_EXERCISE_URL + e.photo,
                  e.description,
                  e.id.toString()))
            });
        return SingleChildScrollView(
            child: Column(
                children: posts.length == 0
                    ? [Text('Belum ada Aktivitas Kebugaran Terkini')]
                    : posts));
      }
      return CircularProgressIndicator(); // or some other widget
    },
  );
}
