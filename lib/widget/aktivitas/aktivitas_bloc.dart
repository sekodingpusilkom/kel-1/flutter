import 'package:bloc/bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simas/models/aktivitas_model.dart';
import 'package:simas/models/commons/exception_constant.dart';
import 'package:simas/widget/aktivitas/aktivitas_event.dart';
import 'package:simas/widget/aktivitas/aktivitas_service.dart';
import 'package:simas/widget/aktivitas_upload/aktivitas_state.dart';

class AktivitasBloc extends Bloc<AktivitasEvent, AktivitasState> {
  AktivitasService _aktivitasService = new AktivitasServiceImpl();
  AktivitasBloc() : super(AktivitasInitial());
  @override
  Stream<AktivitasState> mapEventToState(AktivitasEvent event) async* {
    if (event is AktivitasSharePressed) {
      yield* _mapEventToState(event);
    } else if (event is AktivitasGetData) {
      yield* _mapGetData(event);
    }
    else if(event is AktivitasDeletePressed){
      yield* _mapDeleteData(event);
    }
    else if(event is AktivitasEditPressed){
      yield* _mapEditData(event);
    }

  }

  Stream<AktivitasState> _mapEventToState(AktivitasSharePressed event) async* {
    yield AktivitasLoading();
    try {
      String filePathAndName =
          await _aktivitasService.shareToSocMed(event.imgUrl);
      yield AktivitasShareSuccess(filePathAndName, event.desc);
    } catch (err) {
      yield AktivitasFailure(ExceptionConstant.AKT_FAILED_SHR);
    }
  }

  Stream<AktivitasState> _mapGetData(AktivitasGetData event) async* {
    yield AktivitasLoading();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      List<AktivitasModel> aktModels =
          await _aktivitasService.getAktivitasData(prefs.getString("token"));
      yield AktivitasGetDataSuccess(aktModels);
    } catch (err) {
      yield AktivitasFailure(ExceptionConstant.GEN_FAILED_LOAD);
    }
  }

  Stream<AktivitasState> _mapEditData(AktivitasEditPressed event) async* {
    yield AktivitasLoading();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      await _aktivitasService.editAktivitasData(event.postId, event.desc, event.imgUrl, event.userId,prefs.getString("token"));
      yield AktivitasEditDeleteSuccess(ExceptionConstant.AKT_SUCCESS_EDIT);
    }
    catch (err) {
      yield AktivitasFailure(ExceptionConstant.AKT_FAILED_EDIT);
    }
  }

  Stream<AktivitasState> _mapDeleteData(AktivitasDeletePressed event) async* {
    yield AktivitasLoading();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      await _aktivitasService.deleteAktivitasData(prefs.getString("token"), event.postId);
      yield AktivitasEditDeleteSuccess(ExceptionConstant.AKT_SUCCESS_DEL);
      yield AktivitasInitial();
    }
    catch (err) {
      yield AktivitasFailure(ExceptionConstant.AKT_FAILED_DEL);
    }
  }
}
