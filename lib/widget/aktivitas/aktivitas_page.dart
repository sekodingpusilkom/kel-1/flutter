import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:share/share.dart';
import 'package:simas/models/commons/method_common.dart';
import 'package:simas/widget/aktivitas/aktivitas_bloc.dart';
import 'package:simas/widget/aktivitas/aktivitas_event.dart';
import 'package:simas/widget/aktivitas_upload/aktivitas_state.dart';
import 'package:simas/widget/aktivitas_upload/aktivitas_upl_page.dart';
import 'package:simas/widget/general/appbar_widget.dart';

import 'aktivitas_contain.dart';

class AktivitasPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<AktivitasBloc>(
        create: (context) => AktivitasBloc(), child: _AktivitasPage());
  }
}

class _AktivitasPage extends StatefulWidget {
  @override
  __AktivitasPageState createState() => __AktivitasPageState();
}

class __AktivitasPageState extends State<_AktivitasPage> {
  late AktivitasBloc _bloc;
  @override
  void initState() {
    super.initState();
    _bloc = BlocProvider.of<AktivitasBloc>(context)..add(AktivitasGetData());
  }

  Widget build(context) {
    var child;
    return BlocListener<AktivitasBloc, AktivitasState>(listener:
        (context, state) async {
      if (state is AktivitasFailure) {
        showDialogBox(context, state.error);
      }
      else if(state is AktivitasShareSuccess){
        final RenderBox box = context.findRenderObject() as RenderBox;
        {
          await Share.shareFiles([state.filePath],
              subject: "Aktivitas saya baru-baru ini",
              text: "Aktivitas saya baru-baru ini " + state.desc,
              sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
        }
      }
      else if(state is AktivitasEditDeleteSuccess){
        showDialogBox(context, state.message);
        _bloc.add(AktivitasGetData());
      }
    },
      child:
          BlocBuilder<AktivitasBloc, AktivitasState>(builder: (context, state) {
        print('masuk sini');
        print(state);
        if (state is AktivitasGetDataSuccess) {
          child = aktivitasContain(context, state.aktivitasModels);
        } else if (state is AktivitasLoading) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        return Scaffold(
          appBar: headerNav(),
          body: SafeArea(child: child ?? Text('loading')),
          floatingActionButton: FloatingActionButton(
            onPressed: () async => await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => AktivitasUplPage(),
                )),
            child: Icon(Icons.add),
          ),
        );
      }),
      // bottomNavigationBar: theBottomNav(),
    );
  }
}
