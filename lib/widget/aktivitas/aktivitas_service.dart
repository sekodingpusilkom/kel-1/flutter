import 'dart:io';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as Path;
import 'package:simas/models/aktivitas_model.dart';
import 'package:simas/models/commons/exception_constant.dart';
import 'package:simas/models/commons/url_constant.dart' as ConstantUrl;

abstract class AktivitasService {
  Future<String> shareToSocMed(String imgUrl);

  Future<List<AktivitasModel>> getAktivitasData(String? token);

  Future<bool> deleteAktivitasData(String? token, String postId);

  Future<bool> editAktivitasData(String postId, String desc, String imgUrl, String userId, String? token);
}

class AktivitasServiceImpl implements AktivitasService {
  Future<String> shareToSocMed(String imgUrl) async {
    try {
      var documentDirectory = await getApplicationDocumentsDirectory();
      var firstPath = Path.join(documentDirectory.path, "images_share");
      String dateNow =
          DateFormat('yyyy-MM-dd-hh-mm-ss').format(DateTime.now()).toString();
      var response = await get(Uri.parse(imgUrl));
      var filePathAndName = Path.join(documentDirectory.path, 'images_share');
      filePathAndName = Path.join(filePathAndName, dateNow + '.jpg');
      await Directory(firstPath).create(recursive: true);
      File file2 = new File(filePathAndName);
      file2.writeAsBytesSync(response.bodyBytes);
      return filePathAndName;
    } catch (err) {
      throw Exception(ExceptionConstant.AKT_FAILED_SHR);
    }
  }

  Future<List<AktivitasModel>> getAktivitasData(String? token) async {
    Client client = GetIt.instance.get<Client>();

    var response = await client.get(Uri.parse(ConstantUrl.EXERCISE),
        headers: {
          "content-type": "application/json",
          "accept": "application/json",
          "authorization": "Bearer $token"
        }
        );

    if (response.statusCode == 200) {
      // var resStream = await Response.fromStream(response);
      try {
        return aktivitasModelFromJson(response.body);
      }
      catch(err){
        throw Exception(ExceptionConstant.GEN_FAILED_LOAD);
      }
    } else {
      throw Exception(ExceptionConstant.GEN_FAILED_LOAD);
    }
  }

  Future<bool> deleteAktivitasData(String? token, String postId) async {
    final request =
    new MultipartRequest("DELETE", Uri.parse(ConstantUrl.EXERCISE + '/' + postId));
    request.headers['authorization'] = "Bearer $token";

    var response = await request.send();
    if (response.statusCode == 200) {
        return true;
    }
    else if(response.statusCode == 404){
      throw Exception(ExceptionConstant.AKT_NOT_FOUND);
    }
    else {
      throw Exception(ExceptionConstant.AKT_FAILED_DEL);
    }
  }


  Future<bool> editAktivitasData(String postId, String desc, String imgUrl, String userId, String? token) async {
    var filePathAndName = "";
    try {
      var documentDirectory = await getApplicationDocumentsDirectory();
      var firstPath = Path.join(documentDirectory.path, "images_edit");
      String dateNow =
      DateFormat('yyyy-MM-dd-hh-mm-ss').format(DateTime.now()).toString();
      var response = await get(Uri.parse(imgUrl));
      filePathAndName = Path.join(documentDirectory.path, 'images_edit');
      filePathAndName = Path.join(filePathAndName, dateNow + '.jpg');
      await Directory(firstPath).create(recursive: true);
      File file2 = new File(filePathAndName);
      file2.writeAsBytesSync(response.bodyBytes);
    } catch (err) {
      throw Exception(ExceptionConstant.AKT_FAILED_EDIT);
    }

    final request = new MultipartRequest("PUT", Uri.parse(ConstantUrl.EXERCISE + '/' + postId));
    request.headers['authorization'] = "Bearer $token";

    request.headers['content-type'] = "multipart/form-data";
    request.fields['users_id'] = userId;
    request.fields['description'] = desc;
    request.fields['exercise_id'] = postId;
    request.files.add(await MultipartFile.fromPath(
      'photo',
      filePathAndName,
      contentType: new MediaType('image', 'jpg'),
    ));

    var response = await request.send();

    if (response.statusCode == 200) {
      return true;
    }
    else if(response.statusCode == 404){
      throw Exception(ExceptionConstant.AKT_NOT_FOUND);
    }
    else if(response.statusCode == 400){
      throw Exception(ExceptionConstant.AKT_FAILED_EDIT);
    }
    return false;
  }
}
