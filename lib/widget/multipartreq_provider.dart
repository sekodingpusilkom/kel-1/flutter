
import 'package:http/http.dart';

abstract class BaseMultiPartReq{
  sendRequest(MultipartRequest request);
}

class MultiPartReqImpl implements BaseMultiPartReq{
  @override
  sendRequest(MultipartRequest request) async {
    return await request.send();
  }
}