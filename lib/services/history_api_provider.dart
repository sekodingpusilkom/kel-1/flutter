import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simas/models/history.dart';
import 'package:http/http.dart' as http;
import 'package:simas/models/commons/url_constant.dart' as ConstantUrl;

class HistoryApiProvider {
  final successCode = 200;
  List<HistoryClockIn> parseResponse(http.Response response) {
    if (response.statusCode == successCode) {
      return historyClockInFromJson(response.body);
    } else {
      throw Exception('failed to load HistoryClockIn');
    }
  }

  Future<List<HistoryClockIn>> fetchHistoryClockIn(
      String? userId, DateTime startDate, DateTime endDate) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString("token");
    String formatDate(DateTime date) =>
        new DateFormat("yyyy-MM-dd").format(date);
    // var client = Client();
    final response = await http.get(
        ConstantUrl.historyClockIn(
            userId, formatDate(startDate), formatDate(endDate)),
        headers: {
          "authorization": "Bearer $token"
        });
    return parseResponse(response);
  }
}
